import java.util.*;
class Item			//class for Item
{
    private static int count = 0;		//static variable for counting number of objects created 
    private int rate;					//variable for storing rate
    private String name;				//Variable for storing name
    void set_Name(String x)	
    {								//	function for setting name into variable name
        name = x ; 
    }
    
    String get_Name()
    {								//	function for getting(returning) name
       return name;
    }
    
    void set_Rate(int a)
    {								// function for setting rate into variable rate
        rate = a ;
    }
    
    int get_Rate()
    {								// function for getting (returning) rate
        return rate;
    }
    Item()
    {
        count++ ; 					//constructor for increasing number of items created 
    }
    	
    static int get_Item()
    {
        return count;			//function for returning total numbers of items created 	
    }
}
public class ItemStatic
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        char again;				//for run do while loop again
        String s1;
        String Name;
        int Rate;				//saving rate into variable Rate
        System.out.println("\n Please press Enter to run the program");
        do
        {
        Item i1 = new Item();			//creating new member
        s1 = input.nextLine();
        System.out.print("\n\nEnter name of the Product: ");	
        Name = input.nextLine();				// getting name of product
        i1.set_Name(Name);
        System.out.print("\n\nEnter Rate of the Product: ");
        Rate = input.nextInt();					//getting rate of product
        i1.set_Rate(Rate);
		System.out.println("\n****************************************************");
		System.out.println("\nNumber of Item Created : "+ Item.get_Item());				//total number of items created
        System.out.print("\nName of the given product : "+ i1.get_Name());				//name of product
        System.out.print("\nRate of the given product is : "+ i1.get_Rate());			//rate of the product
		System.out.println("\n****************************************************");	
        System.out.print("\nPress Y if you want to add another product: ");				//checking if user wants to add other item or not
	    again = input.next().charAt(0);
        }while(again == 'y' | again == 'Y');
        System.out.print("\nThe object has created " + Item.get_Item() + " time(s)\nThank you..\n");			//total number of items created
    }  
}
