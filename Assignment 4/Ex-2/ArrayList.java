import static java.lang.System.exit;
import java.util.*;

class List
{
    private int size,curr=0,i,j,temp,count;
    float temp1;
    Scanner input = new Scanner(System.in) ;
    private int []array;
   
     //constructor - getting size of array
    List()             
    {
        System.out.print("Enter Max length of the Array: ");
        size = input.nextInt();
        array = new int[size];
    }
    
    //returning size of array
    int getSize()
    {
        return size;
    }
    
    //returns position of last element in array
    int getPos()
    {
        return curr;
    }
    
   //adding item in array
    public int setItem(int no)
    {
        if(curr<size)
        {
            array[curr]=no;
            curr++;
            return (curr);
        }
        else
        {
            System.out.println("\n The Array is full !!\n");
            return 0;
        }
    }
    
    //deleting by position
    int delByPos(int pos)
    {
        if (pos<1)
       {
            System.out.println("\n Please enter proper number\n");
            return 0;
       }
        if(pos>curr)
        {
            System.out.println("\n The position you want to delete is not in the list rigth now.\n");
            return 0;
        }
        if(pos==curr)
        {
            curr--;
            System.out.println("\n The number " + array[pos-1] + " which is at position " + pos + " has been removed from the list\n");
            return pos;
        }
        if(pos<curr)
        {
            temp = array[pos-1];
            for(i=pos ; i<curr ; i++)
            {
                array[i-1]=array[i];
            }
            curr--;
            System.out.println("\nThe number " + temp + " which is at position " + pos + " has been removed from the list\n");
            return pos;
        }return 0;
    }
    
    //deleting by number 
    void delByNumber(int num)
    {
        for(i=0;i<curr;i++)
        {
            if(num!=array[i])
            {}
            else 
            {
                delByPos(i+1);
            }
        }
    }
    
    //Printing the List
    void printList()
    {
            System.out.println("|--------------------------------------------------|");
            System.out.println("|      Position      |       Value of item         |");
            System.out.println("|--------------------------------------------------|");
        for(i=0;i<curr;i++)
        {
            System.out.println("|         " + (i+1) + "          |           " + array[i] + "                 |");
            
        }
            System.out.println("|--------------------------------------------------|");
    }
    
    //Checking Duplicate
    void duplicate()
    {
        int fr[]=new int[curr];
        for(i=0;i<fr.length;i++) 
        { 
            fr[i]=0;
        }
        // calculating frequency
        for(i=0;i<curr;i++)
        { 
            for(j=0;j<curr;j++)
            { 
                if(array[i]==array[j]) 
                {
                    fr[i]++; 
                }
            }
        } 
            System.out.println("|------------------------------------------------------|");
            System.out.println("| Position | Value of item  | Duplicate? |   Frequency |");
            System.out.println("|------------------------------------------------------|");
        for(i=0;i<curr;i++) 
        { 
            if(fr[i] != 1 )
                System.out.println("|    " + (i+1) + "     |       " + array[i] + "        |     YES    |      " + fr[i] +"      |");
            if(fr[i] == 1 )
           System.out.println("|    " + (i+1) + "     |       " + array[i] + "        |     NO     |      " + fr[i] +"      |");
             
        }   System.out.println("|------------------------------------------------------|");
    }
    
    //removing duplicate
    void rmvDuplicate()
    {
        System.out.println("  BEFORE REMOVIING");
        duplicate();
        System.out.println("  AFTER REMOVIING");
        int fr[]=new int[curr];
        for(i=0;i<fr.length;i++) 
        { 
            fr[i]=0;
        }
        // calculating frequency
        for(i=curr-1;i>=0;i--)
        { 
            for(j=curr-1;j>=0;j--)
            { 
                if(i!=j)
                {
                    if(array[i]==array[j]) 
                    {
                        delByPos(i+1);                  //deleting using delete by position function 
                    }
                }
            }
        } 
        duplicate();
    }
    
    //Sort ascending
    void ascendingSort()
    {
        for (i = 0; i<(curr-1); i++)
        {
            for (j = i+1; j < curr ; j++)
            {
                if (array[i] > array[j])
                {
                    temp = array[j];
                    array[j] = array[i];
                    array[i] = temp; 
                }
            }
        }
        System.out.println("\n\n   The List has been sorted ascendingly successfully.\n\n");
        printList();
    }
    
    //Sort descending
    void descendingSort()
    {
        for (i = 0; i<(curr-1) ; i++)
        {
            for (j = i+1; j < curr ; j++)
            {
                if (array[i] < array[j])
                {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp; 
                }
            }
        }
        System.out.println("\n\n   The List has been sorted descendingly successfully.\n\n");
        printList();
    }
    
    //reversing the list
    void reverseList()
    {
        for (i = 0; i < curr / 2; i++)
        {
        temp = array[i];
        array[i] = array[curr - 1 - i];
        array[curr - 1 - i] = temp;
        }
        System.out.println("\n\n   The Array has been reversed successfully.\n\n");
		printList();
    }
    
    //Maximum
    int max()
    {
        temp = array[0];  
        for(i=1;i < curr;i++)
        {  
            if(array[i] > temp)
            {  
                temp = array[i];  
            }  
        }  
        return temp;  
    }
    
    //minimum
    int min()
    {
        temp = array[0];  
        for(i=1;i<curr;i++)
        {  
            if(array[i] < temp)
            {  
                temp = array[i];  
            }  
        }  
        return temp;
    }
    
    //Sum
    int sum()
    {
        temp=0;
        for(i=0;i<curr;i++)
        {
            temp = temp + array[i] ;
        }
        return temp;
    }
    
    float mean()
    {
        return (sum()/curr);
    }
    
    int median()
    {
        temp = curr / 2;
        if (curr % 2 == 0)
        {
            int left = array[temp - 1];
            int right = array[temp];
            return ((left + right) / 2);
        }
        else
        {
           return array[temp];
        }
    }
    
    int mode()
    {
        int val = 0;
        for (i = 0; i < curr; ++i) 
        {
            temp = 0;
            for (j = 0; j < curr; ++j) 
            {
                if (array[j] == array[i]) ++temp;
            }
            if (temp > count) 
            {
                count = temp;
                val = array[i];
            }
        }
        return val; 
    }
    
    double sd()
    {
        double sd = 0;
        for (i = 0; i < curr ; i++)
        {
            sd += Math.pow((array[i] - mean() ),2) / curr;
        }
        double standardDeviation = Math.sqrt(sd);
        return standardDeviation;
    }
    
    //Calculation
    void calC()
    {
        System.out.println("***************************************************");
        System.out.println(" 1) Maximum");
        System.out.println(" 2) Minimum");
        System.out.println(" 3) Sum");
        System.out.println(" 4) Mean");
        System.out.println(" 5) Median");
        System.out.println(" 6) Mode");
        System.out.println(" 7) Standard Devition");
        System.out.println(" 8) Exit");
        System.out.println("***************************************************");
        System.out.print(" Enter Your Choice : ");
        temp = input.nextInt();
        switch(temp)
        {
            case 1: System.out.println("\n\n The Maximum value from the array is " + max() + "\n\n") ; break;
            case 2: System.out.println("\n\n The Minimum value from the array is " + min() + "\n\n"); break;
            case 3: System.out.println("\n\n The Sum of all the elements of array is " + sum() + "\n\n"); break;
            case 4: System.out.println("\n\n The mean of all the elements of array is " + mean() + "\n\n"); break;
            case 5: System.out.println("\n\n The median of all the elements of array is " + median() + "\n\n"); break;
            case 6: System.out.println("\n\n The mode of all the elements of array is " + mode() + "\n\n"); break;
            case 7: System.out.println("\n\n The Standard Deviation of all the elements of array is " + sd() + "\n\n"); break;
            case 8: exit (0);
            default : System.out.println("Wrong Choice !!! Please enter a proper choice");
        }
    }
}
public class ArrayList
{
    
    public static void main(String[] args)
    {
        int choice,size,i,item,temp,ch1;
        Scanner in = new Scanner(System.in) ;           //scanner declaration
        List lst1;
        lst1 = new List();                           //creating object of class List having int size number of array members
        size = lst1.getSize(); 
        while(true)
        {
            System.out.println("***************************************************");
            System.out.println(" 1) Add an item in the list");
            System.out.println(" 2) Delete an item from the list");
            System.out.println(" 3) Show the List");
            System.out.println(" 4) Check Duplicate element(s) in the list");
            System.out.println(" 5) Remove duplicate values of element");
            System.out.println(" 6) Sort he list in ascending order");
            System.out.println(" 7) Sort he list in descending order");
            System.out.println(" 8) Reverse the list");
            System.out.println(" 9) Calculate max., min., sum, mean, med., mode, and S.D.");
            System.out.println(" 10) Exit ");
            System.out.println("***************************************************");
            System.out.print(" Enter Your Choice:");
            choice = in.nextInt();
            switch (choice)
            {
                case 1: System.out.print(" Enter Number which you want to add : ");
                        item = in.nextInt();
                        temp=lst1.setItem(item);
                        if(temp>0)
                               System.out.println("\n\n The number " + item + " has been stored in the position " + temp + " Successfully.\n\n");
                        break;
                case 2: 
                        System.out.print(" 1) Delete by position\n 2) Delete by Value of number\n Enter your choice : ");       //delete an item
                        ch1 = in.nextInt();
                        switch (ch1)
                        {
                            case 1:System.out.print(" Enter position number of array which you want to delete: ");
                                   temp=in.nextInt();
                                   lst1.delByPos(temp);
                                    break;
                            case 2:System.out.print(" Enter number you want to delete: ");
                                   temp=in.nextInt();
                                   lst1.delByNumber(temp);
                                   break;
                            default: System.out.println(" Wrong Choice !!! Please enter a proper choice");
                        }
                        break;
                case 3: lst1.printList();         break;           //printing the list
                case 4: lst1.duplicate();         break;          //check duplicate
                case 5: lst1.rmvDuplicate();      break;          //remove duplicate
                case 6: lst1.ascendingSort();     break;          //sort ascending
                case 7: lst1.descendingSort();    break;          //sort descending
                case 8: lst1.reverseList();       break;          //reverse list
                case 9: lst1.calC();              break;          //calculate
                
                case 10: exit (0);
                default : System.out.println("Wrong Choice !!! Please enter a proper choice");
            }  
            
        }
    }
	
}