package assignment.pkg7;
import java.util.Scanner;   
public class Assignment7 
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);     //Scanner declaration
        int choice,i,count = 0,overdraft,choice1;
        int temp , maxAccount, month , year , month1 , year1;
        double deposit;
        String accNum;
        boolean check;
        float interest;
        
        /*
         First it will ask for maximum numbers of accounts can be created.
            Which will be used to initialize the array, which will store the objects.
        */
        System.out.print(" Enter Maximum accounts can be created in bank : ");
        maxAccount = input.nextInt();
        Account []accArray = new Account[maxAccount];    //initialization of array
        /*
            Then it will ask for current month and year which will be passed by function while creating an object.
        */
        System.out.print(" Enter Current Month : ");
        month = input.nextInt();
        System.out.print(" Enter Current Year : ");
        year = input.nextInt();
        
        
        while (true)
        {
            System.out.println("****************************************");
            System.out.println(" 1) Create an Account ");
            System.out.println(" 2) Deposit money ");
            System.out.println(" 3) withdraw Money ");
            System.out.println(" 4) Change Overdraft limit ");
            System.out.println(" 5) Check Balance ");
            System.out.println(" 6) Add Interest to all account.");
            System.out.println(" 7) Change Interest rate");
            System.out.println(" 8) Exit ");
            System.out.println("****************************************");
            System.out.print(" Enter Your Choice : ");
            choice = input.nextInt();
            switch(choice)
            {
                case 1: //Create an account
                        /*
                        First it will ask for which type of current you want to make. and then it is
                            implemented using switch case.
                        */
                        System.out.println(" 1) Savings Account ");
                        System.out.println(" 2) Current Account ");
                        System.out.print(" Enter Your Choice : ");
                        choice1 = input.nextInt();
                        switch (choice1)
                        {           
                            case 1: //Savings Account creation
                                    System.out.print(" Enter Account Number :");    
                                    accNum = "1250" ;       // Initialize accNum by a random String (Which solves an error)
                                    accNum = input.next();
                                    /*
                                    In Savings Account,
                                    The minimum value of deposit should be greater than 10,000
                                    Otherwise it will ask again for giving value of deposit.
                                    */
                                    do
                                    {
                                        System.out.print(" Enter Deposit amount : ");
                                        deposit = input.nextDouble();
                                        if(deposit>=1000)
                                            check = false;
                                        else
                                        {
                                            System.out.println(" Deposit CanNot be less than 1,000 rs. ");
                                            check = true ;
                                        }
                                            
                                    }while(check);
                                    
                                    /*
                                    creation of object in array accArray[] by passing value of account number 
                                        and deposite amount.
                                    */
                                    accArray[count] = new Savings(accNum , deposit);
                                    
                                    /*
                                    Passing value of date created (asked in starting)
                                    */
                                    accArray[count].setDate(month, year);
                                    System.out.println(" Account created Successfully.");
                                    count++;    //count shows the number of accounts(objects) created.
                                    break;
                            case 2: //Current Account creation
                                    System.out.print(" Enter Account Number :");
                                    accNum = input.next();
                                    System.out.print(" Enter Deposit amount : ");
                                    deposit = input.nextLong();
                                    //deposite cannot be negative in current account.
                                    // If the entered amount is zero, then it will give message and asks for a proper amount.
                                    do
                                    {
                                        if(deposit >= 0)
                                            check = false ;     //to stop the while loop
                                        else    
                                        {
                                            System.out.println(" Deposit CanNot be negative ");
                                            check = true;       //to start the while loop again
                                        }
                                    }while(check);
                                    /*
                                    Range of overdraft limit is from 500 to 10000000.
                                    This should be implemented in main() only. 
                                    Otherwise the object will be created if we put this constrain in the class.
                                    */
                                    do
                                    {
                                        System.out.print(" Enter overdraft limit : ");
                                        overdraft = input.nextInt();
                                        if(overdraft < 500 || overdraft > 10000000)
                                        {
                                            System.out.println(" Overdraft should be from 500 to 1 crore.");
                                            check = true ;      //to stop the while loop
                                        }
                                        else
                                        check = false ;         //to start the while loop again
                                    }while(check);
                                    accArray[count] = new Current(accNum , deposit , overdraft);
                                    accArray[count].setDate(month, year);       //Setting date of creation which was asked in the beginning.
                                    System.out.println(" Account created Successfully.");
                                    count++;
                                    break;
                            default: System.out.println(" Wrong Choice!!! Please Enter correct choice.");   //default if the input is other then 1 or 2.
                        }
                        break;
                case 2: //Deposite money
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        temp = -1;  //temp shows that at which number in the array, the object of the given account number is stored.
                                    //therefore -1 is pre-set number, which shows that the entered number is not equal to account number of any object.
                        for(i=0 ; i<count ; i++)
                        {
                            //if the entered number and account number of any object is same, then the object number in array is stored in temp.
                            if (accArray[i].getAccountNumber().equals(accNum))
                                temp = i;                            
                        }
                        if(temp == -1)      //-1 is pre-set number, hence if temp remains -1, then the account number must be wrong.
                        {
                            System.out.println(" The Account Number is Wrong.");
                        }
                        else    
                        {
                            System.out.print(" Enter Deposit Amount : ");
                            deposit = input.nextDouble();
                            if (accArray[temp].deposite(deposit))       //the deposited amount will be stored in the object we found above.
                                System.out.println(" The amount has deposited successfully.");
                            else            //when the function returns false(in case of negative deposit)
                                System.out.println(" Amount has not deposited.");
                        }
                        break;
                case 3: //withdrawing money
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        temp = -1;          //this logic is same as above(139-152)
                        for(i=0 ; i<count ; i++)
                        {
                            if (accArray[i].getAccountNumber().equals(accNum))     //same logic as above(139-152)
                                temp = i;                            
                        }
                        if(temp == -1)
                            System.out.println(" The Account Number is Wrong.");
                        else        
                        {
                            System.out.print(" Enter amount to withdraw : ");
                            deposit = input.nextLong();             //here I have reused the double deposit
                                                                    //otherwise I need to make a new double called withdraw. 
                            if (accArray[temp].withdraw(deposit))      //passing amount to the function
                                System.out.println(" The Amount " + deposit + " has been withdrawn successfully.");     //success message in case of true.
                            else        //unsuccess message in case of false
                                System.out.println(" Withdraw Unsuccessful.");
                        }
                        break;
                case 4: //change overdraft limit
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        temp = -1;      //logic of 186-194 is same as above.
                        for(i=0 ; i<count ; i++)
                        {
                            if (accArray[i].getAccountNumber().equals(accNum))
                                temp = i;                            
                        }
                        if(temp == -1)
                            System.out.println(" The Account Number is Wrong.");
                        else
                        {
                            if(accArray[temp].getType())        
                            /*
                                getting type of account(savings or current)
                                This function in savings class returns false and in current class, returns true.
                                In case of Savings, there is nothing like overdraft, hence if function returns true, then only we can change the overdraft limit
                                Otherwise it will gives message line-212
                            */
                            {                  
                                System.out.print(" Enter new overdraft limit : ");
                                overdraft = input.nextInt();
                                if (accArray[temp].setOverdraft(overdraft))
                                    System.out.println("Overdraft changed Successfully.");
                                else
                                    System.out.println(" Overdraft limit is out of range.");
                            }
                            else
                                System.out.println(" Can not change Overdraft in Savings Account.");
                        }
                            
                        break;
                case 5: //check Balance
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        temp = -1;      //logic of line 218 to 225 is same as above.
                        for(i=0 ; i<count ; i++)
                        {
                            if (accArray[i].getAccountNumber().equals(accNum))
                                temp = i;                            
                        }
                        if(temp == -1)      //if the entered number does not matches any of the numbers,(temp = -1) then it shows that number is wrong
                            System.out.println(" The Account Number is Wrong.");
                        else       // If the entered account number matches any of account number, then it will shows the balance of the account.
                            System.out.println(" Balance is rs. " + accArray[temp].getAmount());
                        break;
                case 6: //add interest to every account.
                        System.out.print(" Enter Current Month : ");
                        month1 = input.nextInt();
                        System.out.print(" Enter Current Year : ");
                        year1 = input.nextInt();
                        check = false;
                        /*
                        preset value of check is false, if the last account in savings returns true, then it shows that interest has been added,
                            otherwise, the interest has not been added.
                        */
                        for(i=0;i<count;i++)
                        {
                            if(!accArray[i].getType())      //adding interest to every account.
                                check = accArray[i].addInterest(month1, year1);     //check saves return value.
                        }
                        if(check)
                            System.out.println(" The Interest has been added to every account.");
                        else
                            System.out.println(" The Interest has not been added.");
                        break;
                case 7: // Change interest rate 
                        System.out.print(" Enter new Interest rate : ");
                        interest = input.nextFloat();
                        /*
                        The default value of interest is 6,
                        the entered amount is passed to the function setInterest() function.
                        the interest is static, but we do not know in which place of array, there is an savings account. therefore we have to use all elements of array
                        */
                        for(i=0;i<count;i++)
                        {
                            accArray[i].setInterest(interest);
                        }
                        System.out.println(" The interest rate has been changed successfully.");
                        break;
                case 8: System.exit(0); //exit
                default:System.out.println(" Wrong Choice!!! Please Enter correct choice.");
            }
        }
        
        
       
    }    
}
