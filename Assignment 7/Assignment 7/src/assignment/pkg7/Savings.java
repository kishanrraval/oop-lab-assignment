
package assignment.pkg7;
/*--------------------------------------------------
    - The class Savings is child class of class Account.
---------------------------------------------------*/
public class Savings extends Account
{
/*--------------------------------------------------
    - Here data members of the class are
    interest: having data type float. It contains
        the rate of interest in (savings)account.
        The interest is same for all the accounts,
        hence it is taken as static data member.
---------------------------------------------------*/     
    private static float interest = 6;
    
/*--------------------------------------------------
    minBal: It has double data type. It is used while
        while withdrawing money.
        - One cannot have balance less than minimum balance.
---------------------------------------------------*/
    private double minBal = 500 ;
/*--------------------------------------------------
    lastMonth, lastYear : It has int data type.
        - It is used for storing month and year of last
            interest added. As per company rules,
            one(manager) cannot add interest twice in a quarter.
        -For that, this will be used in the function
            addInterest().
---------------------------------------------------*/
    private static int lastMonth = 0;
    private static int lastYear = 0;

/*--------------------------------------------------
Constructor : 
    - Passing values are AccNUmber which is account number,
        which is passed to super constructor(41).
    - One cannot open an account(object) without giving 
        Account Number and deposit money. The class does not
        contain any normal constructor.
    - As per company rules, the deposit money should be 
        greater than or equals to 10,000.
        This logic is applied in main(). But I have also 
        written it in here as well. Which is not necessary.
---------------------------------------------------*/
    Savings (String accNumber , double money)      //constructor
    {
        super(accNumber);
        if (money >= 1000)      //if statement is not necessary
        {
            amount = money ;
        }
    }
    
/*--------------------------------------------------
withdraw() function is used for withdrawing money from 
    particular account.
    - One Cannot withdraw more than money he has and
        the balance in his account should be more or equal to 
        the minimum balance.
    - For that if statement checks whether the deposited amount 
        is exceeding this limit or not. 
    - if the condition goes wrong, the function returns false,
        otherwise, withdrawn money is deducted from account balance 
        and returns true.
---------------------------------------------------*/
    public boolean withdraw(double money)
    {
        if (money <= (amount-minBal))
        {
            amount -= money;
            return true;
        }
        return false;
    }
    
/*--------------------------------------------------
setOverdraft() function takes value (int) as a parameter
    (same as parent class). 
    -Here this function is not as all used in main().
        Because there nothing like overdraft in Savings account.
---------------------------------------------------*/
    public boolean setOverdraft(int od)
    {
        return false;
    }
    
/*--------------------------------------------------
getType() function returns true or false value.
    - I have pre-decided that Savings Class will return 
        false and Current will return true.
    - This function is helpful in main() for
        checking whether the object created is of Savings 
        class or Current while implementing the concept 
        of polymorphism.
---------------------------------------------------*/    
    public boolean getType()
    {
        return false;
    }
    
/*--------------------------------------------------
addInterest() function takes two parameters, which are
   current month and current year
    - It is used for adding interest to the account.
    - As per company rules, one(manager) can not add interest 
        twice in a quarter. 
    - For that I have two data members in class, lastMonth and
        lastYear, Which tells the last date of the quarter 
        on which interest was added.
    - On the first time, while opening an account, the date on which 
        account created is entered.
    - Here the base of month is 12. therefore we can  write in a single number.
        which is month+(year*12). 
    Logic:
    - (125)If the passed month and year are greater than previous transaction and 
        less than (previous transaction-4), then the interest will be added, and returns true.
    - (137)Otherwise, it will return false.
    - Here, interest is counted using the formula I=P*R*N/100. 
    - (129-133) After adding money to account, it will increment the last month by 4.
        but, the base of month is 12. Hence, if month exceeds 12 then we need to increase 
        year by 1. and reduce the value of month by 1.
------------------------------------------------------*/    
    public boolean addInterest(int month , int year)
    {
        if ((lastMonth + lastYear*12) < (month + year*12) && (lastMonth + lastYear*12 + 3) >= (month + year*12))
        {
            amount += (amount * interest * 0.25 / 100);
            lastMonth += 3;
            if(lastMonth >= 12)
            {
                lastMonth -= 12 ;
                lastYear += 1 ;
            }
            return true ;
        }
        
        return false;
    }
    
/*--------------------------------------------------
setDate() function is used for setting default date 
    for the first time of adding interest.
    - It stores the date creation time in lastMonth and lastYear.
    Logic:
        - It has two parameters which are int. One has value of month and 
            other has year. As per company rules one can add interest once 
            in a quarter. It saves the value as start of quarter.
        - for that, if month is from 1 to 3, then it will save 0.
                                if 4 to 6,then it will save 3
            similarly, 7 to 9->6 and 10 to 12->9
        - Year will remain same.
    - It can be implemented in main as well.
---------------------------------------------------*/
    public void setDate(int month , int year)
    {
        int i;
        for(i=0 ; i<4 ; i++)        
        {
            if((month > i*3) && (month <= 3*(i+1)))
                month = i*3;                
        }
        lastMonth = month ;
        lastYear = year ;
    }
    
/*--------------------------------------------------
setInterest() function has float parameter for setting it's 
    value as interest rate. It is used when Bank changes it's
    interest rate.
---------------------------------------------------*/
    public void setInterest(float rate)
    {
        interest =  rate;
    }
}
