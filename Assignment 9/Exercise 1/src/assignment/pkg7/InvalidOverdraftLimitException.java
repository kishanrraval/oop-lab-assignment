/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg7;

/**
 *
 * @author user
 */
class InvalidOverdraftLimitException extends RuntimeException
{
	public InvalidOverdraftLimitException(String message)
	{
		super("Error Details: " + message);
	}
}