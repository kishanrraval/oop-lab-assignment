
package assignment.pkg7;
/*--------------------------------------------------
    - The class Current is child class of class Account.
---------------------------------------------------*/
public class Current extends Account 
{
/*--------------------------------------------------
    - Current class has data member as overdraft.
    - It has data type as int.
    - Which is used while withdrawing money.
---------------------------------------------------*/

    private int overdarft = 500 ;
    
/*--------------------------------------------------
    Constructor:
        - Passing values are account number(string),
            money(double) and overdraft limit.
        - Account number is passed to the constructor of it's super class.
        - As per company rules, one overdraft limit is 
            ranging from 500 to 10,000,000.
        - This logic is implemented at both main and in the constructor.
---------------------------------------------------*/    
    Current(String acc , double money , int od) throws InvalidOverdraftLimitException
    {
        super(acc);
        amount = money ;
        if (od >= 500 && od <= 10000000)        //if statement is not neccessary here.
        {
            overdarft = od ;
        }
        else
        {
            throw new InvalidOverdraftLimitException("Overdraft limit is out of the range.");
        }
    }
    
/*--------------------------------------------------
setOverdraft() function is used for changing value of overdraft limit.
    - If the value passed in the function is in the range of the overdraft, 
        then, it will be passed as data member overdraft and returns true
    - Else it will pass false and value will not change.
---------------------------------------------------*/
    public void setOverdraft(int od) throws InvalidOverdraftLimitException
    {
        if (od < 500 && od > 10000000)
        {
            throw new InvalidOverdraftLimitException("Overdraft limit is out of range.");
        }
        else
            overdarft = od;        
    }

/*-----------------------------------------------------
withdraw() function is used while withdrawing money.
    - Withdraw money cannot be more then the sum of balance available and 
        overdraft limit.
    - If that condition satisfies, than the withdrawn amount will be 
        deducted from the account balance and returns true.
    - Else the function returns false.
-------------------------------------------------------*/
    public void withdraw(double money) throws InvalidWithdrawAmountException
    {
        if (money <= amount+overdarft)
        {
            amount -= money ;
            
        }
        else
        {
            throw new InvalidWithdrawAmountException("Insufficient balance.");
        }
    }
    
/*-----------------------------------------------------------
    getType() function returns true or false value.
    - I have pre-decided that Savings Class will return 
        false and Current will return true
    - Use of it is defined in Savings class.
------------------------------------------------------------*/    
     public boolean getType()
    {
        return true;
    }
     
/*---------------------------------------------------------
addInterst() function is not used in Current type of account.
     therefore, it returns true every time.
---------------------------------------------------------*/     
    public boolean addInterest(int month , int year)
    {
        return true;
    }

/*-------------------------------------------
setDate() and setInterest() is abstract class of Account, but not used here in Current account.
    - therefore it is blank.
----------------------------------------------*/
    public void setDate(int month , int year)
    {}
    
    public void setInterest(float rate){}

}