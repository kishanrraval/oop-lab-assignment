
package assignment.pkg7;
import java.io.*;
/*---------------------------------------------------------
    - We cannot create an object (account) without
    - Specifying it's type which are Savings and Current.
    - Hence, class account is abstract
----------------------------------------------------------*/
public abstract class Account implements Serializable
{    
/*--------------------------------------------------------
    - Data Members of class are accountNo, which is String
        amount which is double.
    - data member amount is protected because it will be
        used by it's child classes while withdrawing money.
        It can also be implemented using setters and getters.
    - accountNo is String because it will not used for any 
        arithmetic operation.
    - 
----------------------------------------------------------*/
    private String accountNo;
    protected double amount = 0;
/*--------------------------------------------------------*
    - Account class does not contain any default constructor
        because one cannot make any object(account) without
        specifying Account Number.
    - Hense, the class contains parametrised constructor
        passing the value of account number.
----------------------------------------------------------*/    
    Account(String number) throws InvalidAccountNumberException      // para. constructor
    {
        if(number.length()==0)
        {
            throw new InvalidAccountNumberException("The Account number cannot be null");
        }
        accountNo = number ;
    }
     
/*----------------------------------------------------*
    - getAmount() function is used for returning amount
        which is deposited in account. 
    - This function can be used for printing value 
        of deposited amount.        
------------------------------------------------------*/
    public double getAmount()
    {
        return amount;
    }
    
/*----------------------------------------------------*
    - getAccountNumber() function is used for returning
        accountNo.
    - It can be used for checking account number in main()
------------------------------------------------------*/    
    public String getAccountNumber()
    {
        return accountNo;
    }
    
/*----------------------------------------------------*
    - Deposit function is used for deposit in account.
    - Here, depositing money is same for both type of 
        account. Hence this function is in the parent class.
    - One can not deposit money less than or equal zero.
        In this case the function is returning false.
    - Otherwise the deposited money is added to the balance(money)
------------------------------------------------------*/    
    public void deposite(double money) throws InvalidDepositAmountException
    {
        if (money>0)
        {
            amount += money ;
        }
        else
        {
            throw new InvalidDepositAmountException("The Deposie amount is not positive.");
        }
    }
    
/*----------------------------------------------------*
    - setInterest(), addINterest()and setDate() is only for 
        Savings account. But here we are using 
        concept of polymorphism. And using 
        reference variable of Account class in main().
        Therefore Putted abstract function in Account class.
-----------------------------------------------------*/
    public abstract void setInterest(float rate);
    public abstract boolean addInterest(int month , int year);    
    public abstract void setDate(int month , int year);
/*----------------------------------------------------*
    - setOverdraft() is only for Current Account. But as 
        given above, it is an abstract function.
------------------------------------------------------*/
    public abstract void setOverdraft(int od);
/*----------------------------------------------------*
    - Withdrawing money has different logic for 
        both type of accounts. Therefore it is an abstract class.
 -----------------------------------------------------*/
    public abstract void withdraw(double money);
/*-----------------------------------------------------
    - getType is used for returning which type of account
        the object has.
    - One type(Savings) will return false and 
        one type(Current) will return true.
 ------------------------------------------------------*/
    public abstract boolean getType();
    
}
