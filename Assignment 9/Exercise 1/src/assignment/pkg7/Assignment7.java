package assignment.pkg7;
import java.util.Scanner; 
import java.io.*;

public class Assignment7
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);     //Scanner declaration
        int choice,i,count = 0,overdraft,choice1,count1=0;
        int temp , maxAccount, month , year , month1 , year1;
        double deposit;
        String accNum;
        
        boolean check,flag;
        float interest;
        
        Account object = null;
        String[] accNumber = new String[100];
        FileOutputStream fout  = null;
        ObjectOutputStream oout = null;
        FileInputStream fin = null;
        ObjectInputStream oin = null;
        File f;
                        
        /*
            Then it will ask for current month and year which will be passed by function while creating an object.
        */
        System.out.print(" Enter Current Month : ");
        month = input.nextInt();
        System.out.print(" Enter Current Year : ");
        year = input.nextInt();
        
        
        while (true)
        {
            System.out.println("\n****************************************");
            System.out.println(" 1) Create an Account ");
            System.out.println(" 2) Deposit money ");
            System.out.println(" 3) withdraw Money ");
            System.out.println(" 4) Change Overdraft limit ");
            System.out.println(" 5) Check Balance ");
            System.out.println(" 6) Add Interest to all account.");
            System.out.println(" 7) Change Interest rate");
            System.out.println(" 8) Delete an Account ");
            System.out.println(" 9) Exit ");
            System.out.println("****************************************");
            System.out.print(" Enter Your Choice : ");
            choice = input.nextInt();
            switch(choice)
            {
                case 1: //Create an account
                        /*
                        First it will ask for which type of current you want to make. and then it is
                            implemented using switch case.
                        */
                        System.out.println(" 1) Savings Account ");
                        System.out.println(" 2) Current Account ");
                        System.out.print(" Enter Your Choice : ");
                        choice1 = input.nextInt();
                        switch (choice1)
                        {           
                            case 1: //Savings Account creation
                                    
                                    
                                    /*
                                    In Savings Account,
                                    The minimum value of deposit should be greater than 10,000
                                    Otherwise it will ask again for giving value of deposit.
                                    creation of object by passing value of account number 
                                        and deposite amount.
                                    */
                                    flag = true;
                                    accNum = "" ;   // Initialize accNum by a random String (Which solves an error)
                                    while(flag)
                                    {
                                        System.out.print(" Enter Account Number :");    
                                               
                                        accNum = input.next();
                                        System.out.print(" Enter Deposit amount : ");
                                        deposit = input.nextDouble();
                                        try
                                        {
                                            object = new Savings (accNum,deposit);          ///creating object and putting the statement in while loop
                                                //the while loop will run until the inpus are correct
                                            System.out.println(" Account created Successfully.");
                                            flag = false;
                                        }
                                        catch(InvalidAccountNumberException ex)     //catching the exception we have creaated
                                        {
                                            ex.printStackTrace();
                                        }
                                        catch(InvalidDepositAmountException ex)
                                        {
                                            ex.printStackTrace();
                                        }
                                    }
                                    
                                    /*
                                    Passing value of date created (asked in starting)
                                    */
                                    object.setDate(month,year);
                                    
                                    try
                                    {
                                        fout = new FileOutputStream(accNum + ".ser");
                                        oout = new ObjectOutputStream(fout);
                                        oout.writeObject(object);
                                        oout.flush();
                                    }
                                    catch(FileNotFoundException ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                    catch(IOException ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                    finally{
                                        try
                                        {
                                            if(fout!=null)
                                            {
                                                oout.close();
                                                fout.close();
                                                oout = null;
                                                fout = null;
                                            }
                                        }
                                        catch(IOException ex)
                                        {
                                            ex.printStackTrace();
                                        }
                                        accNumber[count1] = accNum; 
                                        count1++;
                                    }
                                    break;
                            case 2: //Current Account creation
                                    
                                    
                                    
                                        
                                        //deposite cannot be negative in current account.
                                        // If the entered amount is zero, then it will give message and asks for a proper amount.
                                        
                                        
                                        /*
                                        Range of overdraft limit is from 500 to 10000000.
                                        This should be implemented in main() only. 
                                        Otherwise the object will be created if we put this constrain in the class.
                                        */
                                        
                                        flag = true;
                                        accNum = "";
                                        while(flag)
                                        {
                                            System.out.print(" Enter Account Number :");
                                            accNum = input.next();
                                            System.out.print(" Enter Deposit amount : ");
                                            deposit = input.nextLong();
                                            System.out.print(" Enter overdraft limit : ");
                                            overdraft = input.nextInt();
                                            try
                                            {
                                                object = new Current(accNum , deposit , overdraft);
                                                System.out.println(" Account created Successfully.");
                                                flag = false;
                                            }
                                            catch(InvalidAccountNumberException ex)
                                            {
                                                ex.printStackTrace();
                                            }
                                            catch(InvalidDepositAmountException ex)
                                            {
                                                ex.printStackTrace();
                                            }
                                            catch(InvalidOverdraftLimitException ex)
                                            {
                                                ex.printStackTrace();
                                            }
                                        }
                                        
                                        object.setDate(month,year);
                                        
                                        try
                                        {
                                            fout = new FileOutputStream(accNum + ".ser");
                                            oout = new ObjectOutputStream(fout);
                                            oout.writeObject(object);
                                            oout.flush();
                                        }
                                        catch(FileNotFoundException ex)
                                        {
                                            ex.printStackTrace();
                                        }
                                        catch(IOException ex)
                                        {
                                            ex.printStackTrace();
                                        }
                                        finally{
                                            try
                                            {
                                                if(fout!=null)
                                                {
                                                    oout.close();
                                                    fout.close();
                                                    oout = null;
                                                    fout = null;
                                                }
                                            }
                                            catch(IOException ex)
                                            {
                                                ex.printStackTrace();
                                            }
                                        }
                                        accNumber[count1] = accNum;
                                        count1++;
                                    
                                    break;
                            default: System.out.println(" Wrong Choice!!! Please Enter correct choice.");   //default if the input is other then 1 or 2.
                        }
                        break;
                case 2: //Deposite money
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");
                            oin = new ObjectInputStream(fin);
                            
                            
                            object = (Account) oin.readObject();
                            System.out.print(" Enter Deposit Amount : ");
                            deposit = input.nextDouble();
                            try
                            {
                                object.deposite(deposit);
                                System.out.println(" The amount has deposited successfully.");
                            }       //the deposited amount will be stored in the object we found above.
                            catch(InvalidDepositAmountException ex)
                            {
                               ex.printStackTrace();
                            }

                               
                            
                            fout = new FileOutputStream(accNum + ".ser");
                            oout = new ObjectOutputStream(fout);
                            oout.writeObject(object);
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exist.");
                        break;
                case 3: //withdrawing money
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");
                            oin = new ObjectInputStream(fin);
                            
                            object = (Account) oin.readObject();
                            System.out.print(" Enter amount to withdraw : ");
                            deposit = input.nextLong();             //here I have reused the double deposit
                                                                //otherwise I need to make a new double called withdraw. 
                            try
                            {
                                object.withdraw(deposit);
                                System.out.println(" The Amount " + deposit + " has been withdrawn successfully.");     //success message in case of true.
                            }                //passing amount to the function
                            catch(InvalidWithdrawAmountException ex)
                            {
                                ex.printStackTrace();
                            }
                                
                                
                                                   
                            fout = new FileOutputStream(accNum + ".ser");
                            oout = new ObjectOutputStream(fout);
                            oout.writeObject(object);
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exists.");
                        
                        break;
                case 4: //change overdraft limit
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");
                            oin = new ObjectInputStream(fin);
                            
                            
                            object = (Account) oin.readObject();
                            
                            System.out.print(" Enter new overdraft limit : ");
                            overdraft = input.nextInt();
                            try
                            {
                                object.setOverdraft(overdraft);
                                System.out.println("Overdraft changed Successfully.");
                            }
                            catch(InvalidOverdraftLimitException ex)
                            {
                                ex.printStackTrace();
                            }

                            
                            
                            fout = new FileOutputStream(accNum + ".ser");
                            oout = new ObjectOutputStream(fout);
                            oout.writeObject(object);
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exist.");
                        break;
                case 5: //check Balance
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");
                            oin = new ObjectInputStream(fin);
                            
                            
                            object = (Account) oin.readObject();
                            System.out.print(" Balance in the account is : " + object.getAmount());
                              
                        }
                        catch(ClassNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally
                        {
                            try
                            {
                                oin.close();
                                fin.close();
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                            
                            oin = null;
                            fin = null;
                        }
                        }
                        else
                            System.out.println("Account does not exist.");
                        
                        break;
                case 6: System.out.print(" Enter Current Month : ");
                        month1 = input.nextInt();
                        System.out.print(" Enter Current Year : ");
                        year1 = input.nextInt();
                        check = false;
                        for(i=0 ; i < accNumber.length ; i++)
                        {
                        f = new File(accNumber[i] + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNumber[i] + ".ser");
                            oin = new ObjectInputStream(fin);
                            object = (Account) oin.readObject();
                            
                            if(!object.getType())      //adding interest to every account.
                                check = object.addInterest(month1, year1);
                                                 
                            fout = new FileOutputStream(accNumber[i] + ".ser");
                            oout = new ObjectOutputStream(fout);
                            oout.writeObject(object);
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        
                        }
                        }
                        if(check)
                            System.out.println(" The Interest has been added to every account.");
                        else
                            System.out.println(" The Interest has not been added.");
                    
                    
                    
                    
                        break;
                case 7: System.out.print(" Enter new Interest rate : ");
                        interest = input.nextFloat();
                        for(i=0 ; i < accNumber.length ; i++)
                        {
                        f = new File(accNumber[i] + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNumber[i] + ".ser");
                            oin = new ObjectInputStream(fin);
                            object = (Account) oin.readObject();
                            try
                            {
                                object.setInterest(interest);
                            }
                            catch(InvalidInterestRateException ex)
                            {
                                ex.printStackTrace();
                            }
                                                 
                            fout = new FileOutputStream(accNumber[i] + ".ser");
                            oout = new ObjectOutputStream(fout);
                            oout.writeObject(object);
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        
                        }
                        }
                        System.out.println(" The interest rate has been changed successfully.");
                        break;
                    
                        
                case 8: System.out.print(" Enter Account Number you want to delete : ");
                        accNum = input.next();
                        f = null;
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                            f.delete();
                            System.out.println(" The Account deleted Successfully.");
                        }
                        else
                            System.out.println(" Account does not exist.");
                        break;
                case 9: System.exit(0); //exit
                default:System.out.println(" Wrong Choice!!! Please Enter correct choice.");
            }
        }   
    }      
}

