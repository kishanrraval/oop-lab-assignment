import java.util.Scanner;
import java.io.*;
abstract class Shape implements Serializable
{
	String color;
    	Shape(String colorName)
    	{
     	   color = colorName;
    	}	
    public void setColor(String colorName) throws InvalidColorNameException
    {
        if(colorName.length() == 0)
        {
            throw new InvalidColorNameException("The colour name can not be null.");
        }
        else
            color = colorName;
    }
    public String getColor()
    {
        return color;
    }
	abstract float area();
	abstract float volume();
}

abstract class TwoDimension extends Shape
{
   TwoDimension(String colorName)
    {
        super(colorName);
    }
    abstract float area();
	float volume() throws NoVolumeException
	{
		throw new NoVolumeException(" Can not find volume in two dimension.");
		
	}
}
abstract class threeDimension extends Shape
{
    threeDimension(String colorName)
    {
         super(colorName);
    }
    abstract float area();
    abstract float volume();         
}

class Rectangle extends TwoDimension
{
    private float x1;
    private float y1;
    private float x2;
    private float y2;
    private float x3;
    private float y3;
    Rectangle(String color,float x1,float y1,float x2,float y2,float x3,float y3)
    {
		super(color);
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.x3 = x3;
		this.y3 = y3;
    }
    public float area()
    {
        float area;
        //System.out.print("\nx1: " + x1 + "\nx2: " + x2 + "\nx3: " + x3 + "\ny1: " + y1 + "\ny2: " + y2 + "\ny3: " + y3);
        area = (x1*(y2 - y3) + x2*(y3 - y1) + x3*(y1 - y2))/2;
        if(area<0)
            area = - area;
        return area;
    }
}

class Circle extends TwoDimension
{
    private float x;
    private float y;
    private float radius;
    Circle(String color,float  radius)/*,float  y ,float radius)*/ throws InvalidRadiusException
    {
        super(color);
        //this.x = x;
        //this.y = y; 
        if(radius < 0)
        {
                throw new InvalidRadiusException("The Radius can not be negative.");
        }
        else
                this.radius = radius;
    }
    
    
     Circle(String color,float  radius,float  y ,float rad) throws InvalidRadiusException
	{
		super(color);
        	this.x = x;
        	this.y = y; 
        if(radius < 0)
        {
                throw new InvalidRadiusException("The Radius can not be negative.");
        }
        else
                this.radius = rad;
	}	
	public float area()
	{
		return ((22/7)*radius*radius);
	}
}

class Cuboid extends threeDimension 
{
    private float length, breadth, height;
    Cuboid(String color,float length, float breadth, float height)
    {
            super(color);
            if(length < 0 || breadth < 0 || height < 0)
            {
                    throw new InvalidLengthException(" the length can not be negative.");
            }
            else
            {
                    this.length = length;
                    this.breadth = breadth;
                    this.height = height; 	
            }

    }
    public float area()
    {
            return (2 * (length * breadth + breadth * height + height * length));
    }
    public float volume()
    {
            return (length * breadth * height);
    }    
}

class Sphere extends threeDimension
{
    private float radius,x,y;
	
    Sphere(String color,float x,float y,float radius) throws InvalidRadiusException
    {
            super(color);
            this.x = x;
            this.y = y;
            if(radius < 0)
            {
                    throw new InvalidRadiusException("The Radius can not be negative.");
            }
            this.radius = radius;
    }

    public float area()
    {
            return (4* 22/7 * radius * radius);
    }
    public float volume()
    {
            return (4/3 * 22/7 * radius * radius);
    }
}

class InvalidColorNameException extends RuntimeException		//User defined exceptions
{
    public InvalidColorNameException(String message)
    {
        super(" Exception Details: " + message);
    }
}
class InvalidRadiusException extends RuntimeException
{
	public InvalidRadiusException(String message)
	{
		super(" Exception Details : " + message);
	}
}

class InvalidLengthException extends RuntimeException
{
	public InvalidLengthException(String message)
	{
		super(" Exception Details : " + message);
	}
}
class NoVolumeException extends RuntimeException
{
	public NoVolumeException(String message)
	{
		super(" Exception Details : " + message);
	}
}
public class Ex2
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int choice, choice1;
		float x1,x2,x3,y1,y2,y3;
		String color="";
		
		Shape object = null;
		 FileOutputStream fout  = null;			//object of class FileOutputStream and ObjectOutputStream for Serializing an object
        ObjectOutputStream oout = null;			
        FileInputStream fin = null;				//object of class FileInputStream and ObjectInputStream for de serializing the object
        ObjectInputStream oin = null;			
        File f;									//reference of File class for checking existence of the particular account
		while(true)
		{
			System.out.println("\n*************************************");
			System.out.println(" 1) Create new Shape");
			System.out.println(" 2) Find Area");
			System.out.println(" 3) Find Volume");
			System.out.println(" 4) Exit");
			System.out.println("*************************************");
			System.out.print(" Enter Your Choice : ");
			choice = input.nextInt();
			switch(choice)
			{
				case 1:	System.out.println(" 1) Rectangle");
						System.out.println(" 2) Circle");
						System.out.println(" 3) Cuboid");
						System.out.println(" 4) Sphere");
						System.out.print(" Enter your choice : ");
						choice1 = input.nextInt();
						System.out.println(" Enter the details.");
						System.out.print(" Enter Colour : ");
						color = input.nextLine();			//this solves an error
						color = input.nextLine();
						switch(choice1)
						{
							case 1:	System.out.println(" Enter values of x axes (of all vertices): ");
							//System.out.println(" Enter values of radius: ");
									x1 = input.nextFloat();
									x2 = input.nextFloat();
									x3 = input.nextFloat();
									System.out.println(" Enter values of y axes (of all vertices): ");
									y1 = input.nextFloat();
									y2 = input.nextFloat();
									y3 = input.nextFloat();
									try
									{
										object = new Rectangle(color, x1, y1, x2, y2, x3, y3);
										System.out.println(" The object has created successfully.");
									}
									catch(InvalidColorNameException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									
									break;
							case 2:	System.out.print(" Enter X axes value of centre : ");
									x1 = input.nextFloat();
									System.out.print(" Enter Y axes value of centre : ");
									y1 = input.nextFloat();
									System.out.print(" Enter value of radius : ");
									x2 = input.nextFloat();
									try{
										object = new Circle(color, x1, y1, x2);
										System.out.println(" The object has created successfully.");
									}
									catch(InvalidRadiusException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									catch(InvalidColorNameException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									
									break;
							case 3:	System.out.print(" Enter value of length : ");
									x1 = input.nextFloat();
									System.out.print(" Enter value of breadth : ");
									x2 = input.nextFloat();
									System.out.print(" Enter value of height : ");
									x3 = input.nextFloat();
									try{
										object = new Cuboid(color, x1, x2, x3);
										System.out.println(" The object has created successfully.");
									}
									catch(InvalidLengthException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									catch(InvalidColorNameException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									break;
							case 4:	System.out.print(" Enter X axes value of centre : ");
									x1 = input.nextFloat();
									System.out.print(" Enter Y axes value of centre : ");
									y1 = input.nextFloat();
									System.out.print(" Enter value of radius : ");
									x2 = input.nextFloat();
									try{
										object =  new Sphere(color, x1, y1, x2);
										System.out.println(" The object has created successfully.");
									}
									catch(InvalidColorNameException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									catch(InvalidRadiusException ex)
									{
										ex.printStackTrace();
										System.out.println("Please Try again.");
									}
									
									break;
							default:System.out.println("Wrong Choice!!! Please enter a proper choice.");
						}
						try
						{
							fout = new FileOutputStream(color + ".ser"); 	//passing account name to the FileOutputStream to the constructor
							oout = new ObjectOutputStream(fout);			//creating object of ObjectInputStream
							oout.writeObject(object);						//Writing object data into file
							oout.flush();
						}
						catch(FileNotFoundException ex)			//Catching all the exception generated while serializing object and displaying the Stack Trace
						{
							ex.printStackTrace();
						}
						catch(IOException ex)
						{
							ex.printStackTrace();
						}
						finally			//closing the file objects and assigning null values to the Objects. So, that they can be used again.
						{
							try
							{
								if(fout!=null)
								{
									oout.close();
									fout.close();
									oout = null;
									fout = null;
								}
							}
							catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
							{
								ex.printStackTrace();
							}
						}
						break;
				case 2:	System.out.print(" Enter Shape colour: ");
						color = input.nextLine();
						color = input.nextLine();
						
                        f = new File(color + ".ser");
                        if(f.exists())
                        {
							try
							{
								fin = new FileInputStream(color + ".ser");		//passing account name to the FileInputStream constructor
								oin = new ObjectInputStream(fin);				//creating object of ObjectOutputStream
									//I am getting error here whil typeCasting the object.
								object = (Shape) oin.readObject();			//reading the object data from the file and typecasting it to Account class.			
								System.out.print("The total area of the Shape is : " + object.area());
								  //Printing balance of account using getAmount function
							}
							catch(ClassNotFoundException ex)	//Catching all the exception generated while de-serializing objects and displaying the Stack Trace
							{
								ex.printStackTrace();
							}
							catch(FileNotFoundException ex)
							{
								ex.printStackTrace();
							}
							catch(IOException ex)
							{
								ex.printStackTrace();
							}
							catch(ClassCastException ex)
							{
								
							}
							finally	//closing the file objects and assigning null values to the Objects. So, that they can be used again.
							{
								try
								{
									oin.close();
									fin.close();
								}
								catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
								{
									ex.printStackTrace();
								}
								
								oin = null;
								fin = null;
							}
                        }
                        else
                            System.out.println("Shape does not exist.");
						break;
				case 3:	System.out.print(" Enter Shape colour: ");
						color = input.nextLine();
						color = input.nextLine();
						
                        f = new File(color + ".ser");
                        if(f.exists())
                        {
							try
							{
								fin = new FileInputStream(color + ".ser");		//passing account name to the FileInputStream constructor
								oin = new ObjectInputStream(fin);				//creating object of ObjectOutputStream
								object = (Shape) oin.readObject();			//reading the object data from the file and typecasting it to Account class.			
								System.out.print("The total area of the Shape is : " + object.volume());
								
							}
							catch(ClassNotFoundException ex)	//Catching all the exception generated while de-serializing objects and displaying the Stack Trace
							{
								ex.printStackTrace();
							}
							catch(FileNotFoundException ex)
							{
								ex.printStackTrace();
							}
							catch(IOException ex)
							{
								ex.printStackTrace();
							}
							catch(ClassCastException ex)
							{
								System.out.println("Can not find volume in two dimension.");
								ex.printStackTrace();
							}
							catch(NoVolumeException ex)
							{
								ex.printStackTrace();
							}
							finally	//closing the file objects and assigning null values to the Objects. So, that they can be used again.
							{
								try
								{
									oin.close();
									fin.close();
								}
								catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
								{
									ex.printStackTrace();
								}
								
								oin = null;
								fin = null;
							}
                        }
                        else
                            System.out.println("Account does not exist.");
						break;
				case 4: System.exit(0);
				default: System.out.println("Wrong Choice!!! Please enter a proper choice.");
			}
		}
	}
}
