import java.util.Scanner;
import java.io.*;		//Importing packages and classes
class Ex2 	//main class
{
	public static String file;				//declaring static String file which will hold name of the file.
    public static String fileName()			//function for getting the file name and returning the String.
    {
        Scanner input = new Scanner(System.in);			//creating object of Scanner
        String x;
        System.out.print("\n Enter file name : ");		//Asking for the file name
        x = input.nextLine();
        return x;					 				    //Returning file name
    }
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        int choice,count,count1;			//choice for cases, count for saving number of characters and count1 for saving number of lines
        String fileN = " ",fileN1 = " ";	//fileN for saving file name and fileN1 for saving name for a new file while copying a file
        while(true)
        {
            
            count = 0;count1=0;				//putting all counters at 0
			//Printing menu
            System.out.println("**********************************************");		
            System.out.println(" 1) Count the number of characters");
            System.out.println(" 2) Count the number of words");
            System.out.println(" 3) Count the number of lines");
            System.out.println(" 4) Copy the content from one file to another");
            System.out.println(" 5) Rename a file");
            System.out.println(" 6) Exit ");
            System.out.println("***********************************************");
            System.out.print(" Enter Your Choice : ");
            choice = input.nextInt();
            switch (choice)
            {
                case 1: //Count the number of characters
						fileN = fileName();						//asking for name of fire(with extension)
                        File f = new File(fileN);				//passing name to the file object constructor
                        if(f.exists())							//If the file name exists, then it will do process
                        {
                            FileReader fr = null;				//creating reference of FileReader class
                            try
                            {
                                fr = new FileReader(fileN);				//creating instance of the reference
                                int i = 0;						
                                while((i = fr.read()) != -1)			//getting value from the file
                                {
									//10 is ASCII value for new line
                                    count++;							//incrementing the counter for characters 
                                    if(i == 10)
                                        count1++;						//incrementing the counter for new lines
                                }
                                count -= count1;				//subtracting value of new line from characters because new line is also counted in characters
                            }
                            catch(FileNotFoundException ex)		//catching FileNotFoundException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            catch(IOException ex)				//catching IOException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            finally			
                            {
                                if(fr!=null)					//If the object fr is not null then closing the file and passing null value to the object. 
                                {
                                    try{
                                        fr.close();				//closing the File object
                                        fr = null;
                                    }
                                    catch(IOException ex)		//catching IOException and displaying it's details.
                                    {
                                        ex.printStackTrace();
                                    }
                                }
                            }
                            System.out.println(" Total number of characters are : " + count);   
                        }
                        else
                            System.out.println(" The file does not exist.");		//This statement will be executed when the file name does not exist
                        break;
                case 2: //Count the number of words
						fileN = fileName();			//asking for name of fire(with extension)
                        File f1 = new File(fileN);	//passing name to the file object constructor
						
                        if(f1.exists())				//If the file name exists, then it will do process
                        {
                            FileReader fr = null;	//creating reference of FileReader
                            try
                            {
                                fr = new FileReader(fileN);		//creating instance of FileReader 
                                int i = 0;
                                while((i = fr.read()) != -1)	//getting data from the file
                                {
									//32 is ASCII value of space
									//10 is ASCII value of new line
                                    if(i == 32 || i==10)		//counting the number of words. Here I have added total number of spaces + total number of new lines
                                        count++;
                                }
                            }
                            catch(FileNotFoundException ex)				//catching FileNotFoundException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            catch(IOException ex)						//catching IOException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            finally
                            {
                                if(fr!=null)		//If the object fr is not null then closing the file and passing null value to the object.
                                {
                                    try{
                                        fr.close();		//closing the File object
                                        fr = null;
                                    }
                                    catch(IOException ex)			//catching IOException and displaying it's details.
                                    {
                                        ex.printStackTrace();
                                    }
                                }
                            }
                             System.out.println(" Total number of words are : " + (count+1));   
                        }
                        else
                            System.out.println(" The file does not exist.");	//This statement will be executed when the file name does not exist
                        break;
                case 3: //Count the number of lines
						fileN = fileName();			//asking for name of fire(with extension)
                        File f2 = new File(fileN);	//passing name to the file object constructor
                        if(f2.exists())				//If the file name exists, then it will do process
                        {
                            FileReader fr = null;	//creating reference of FileReader
                            try
                            {
                                fr = new FileReader(fileN);			//creating instance of FileReader
                                int i = 0;
                                while((i = fr.read()) != -1)		//getting data from the file
                                {   
								//10 is ASCII value of new line
                                    if(i == 10)
                                        count++;
                                }
                            }
                            catch(FileNotFoundException ex)				//catching FileNotFoundException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            catch(IOException ex)						//catching IOException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            finally
                            {
                                if(fr!=null)		//If the object fr is not null then closing the file and passing null value to the object.
                                {
                                    try{
                                        fr.close();		//closing the File object
                                        fr = null;
                                    }
                                    catch(IOException ex)			//catching IOException and displaying it's details.
                                    {
                                        ex.printStackTrace();
                                    }
                                }
                            }
                            System.out.println(" Total number of characters are : " + (count+1));   
                        }
                        else
                            System.out.println(" The file does not exist.");		//This statement will be executed when the file name does not exist
                        break;
                case 4: //Copy the content from one file to another
						fileN = fileName();			//asking for name of fire(with extension)
                        File f3 = new File(fileN);	//passing name to the file object constructor
                        System.out.print("Enter File name of new file with path(optional) : ");
                        fileN1 = input.next();
                        if(f3.exists())				//If the file name exists, then it will do process
                        {
                            FileInputStream fin = null;					//creating reference of fileInputStream
                            BufferedInputStream bin = null;				//creating reference of BufferedInputStream
                            FileOutputStream fout = null;				//creating reference of FileOutputStream
                            BufferedOutputStream bout = null;			//creating reference of BufferedOutputStream
                            int i = 0;
                            byte [] arr = new byte[10];					//creating byte array for writing data from bin to bout 
                            try
                            {
                                    
                                fin = new FileInputStream(fileN);		//creating instance of fileInputStream
                                bin = new BufferedInputStream(fin);		//creating instance of BufferedInputStream
                                fout = new FileOutputStream(fileN1);	//creating instance of FileOutputStream
                                bout = new BufferedOutputStream(fout);	//creating instance of BufferedOutputStream
                                
                                int b = 0;
                                
                                while((b = bin.read(arr)) != -1 )		//getting data from bin
                                {
                                    bout.write(arr);					//writing that data to bout
                                }
                                bout.flush();							//successfully saved the data
                                
                            }
                            catch(FileNotFoundException ex)				//catching FileNotFoundException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            catch(IOException ex)						//catching IOException and displaying it's details.
                            {
                                ex.printStackTrace();
                            }
                            finally
                            {
                                try
                                {
                                    if(bin != null)			//If the object fr is not null then closing the file and passing null value to the object.
                                    {
                                        bin.close();	//closing the File object
                                        fin.close();
                                        bin = null;
                                        fin = null;
										bout.close();
										fout.close();
										bout = null;
										fout = null;
                                    }
                                }
                                catch(IOException ex)				//catching IOException and displaying it's details.
                                {
                                    ex.printStackTrace();
                                }
                            }
                            System.out.println(" File copied Successfully.");
                        }
                        else
                            System.out.println("The file does not exist.");		//This statement will be executed when the file name does not exist
                        break;
                case 5: //Rename a file
						fileN = fileName();			//asking for name of fire(with extension)
                        File f4 = new File(fileN);	//passing name to the file object constructor
                        if(f4.exists())				//If the file name exists, then it will do process
                        {
                            System.out.print("Enter new file name : ");			//getting new name for rename a file
                            fileN1 = input.next();
                            File rename = new File(fileN1);						//creating a new object for rename a file
                            if(f4.renameTo(rename))								//using renameTo function for rename a file
                                System.out.println(" The file Renamed Successfully.");	
                            else
                                System.out.println(" Process Unsuccessful.");
                        }
                        else
                            System.out.println(" File does not exist.");		//This statement will be executed when the file name does not exist 
                        break;
                case 6:	//Exit
						System.exit(0);
                default: System.out.println(" Wrong Choice!!! Please enter a proper Choice.");    
            }
        }
    }    
}
