import java.util.Scanner; 
import java.io.*;
/*---------------------------------------------------------
    - We cannot create an object (account) without
		Specifying it's type which are Savings and Current.
		Hence, class account is abstract
	- The class is used for serializing the object hence 
		it implements Serializable class.
----------------------------------------------------------*/
abstract class Account implements Serializable
{
    
/*--------------------------------------------------------
    - Data Members of class are accountNo, which is String
        amount which is double.
    - data member amount is protected because it will be
        used by it's child classes while withdrawing money.
        It can also be implemented using setters and getters.
    - accountNo is String because it will not used for any 
        arithmetic operation.
    - 
----------------------------------------------------------*/
    private String accountNo;
    protected double amount = 0;
/*--------------------------------------------------------*
    - Account class does not contain any default constructor
        because one cannot make any object(account) without
        specifying Account Number.
    - Hence, the class contains parametrised constructor
        passing the value of account number.
----------------------------------------------------------*/    
    Account(String number)      // para. constructor
    {
        accountNo = number ;
    }
     
/*----------------------------------------------------*
    - getAmount() function is used for returning amount
        which is deposited in account. 
    - This function can be used for printing value 
        of deposited amount.        
------------------------------------------------------*/
    public double getAmount()
    {
        return amount;
    }
    
/*----------------------------------------------------*
    - getAccountNumber() function is used for returning
        accountNo.
    - It can be used for checking account number in main()
------------------------------------------------------*/    
    public String getAccountNumber()
    {
        return accountNo;
    }
    
/*----------------------------------------------------*
    - Deposit function is used for deposit in account.
    - Here, depositing money is same for both type of 
        account. Hence this function is in the parent class.
    - One can not deposit money less than or equal zero.
        In this case the function is returning false.
    - Otherwise the deposited money is added to the balance(money)
------------------------------------------------------*/    
    public boolean deposite(double money)
    {
        if (money>0)
        {
            amount += money ;
            return true;
        }
        return false;
    }
    
/*----------------------------------------------------*
    - setInterest(), addINterest()and setDate() is only for 
        Savings account. But here we are using 
        concept of polymorphism. And using 
        reference variable of Account class in main().
        Therefore Putted abstract function in Account class.
-----------------------------------------------------*/
    public abstract void setInterest(float rate);
    public abstract boolean addInterest(int month , int year);    
    public abstract void setDate(int month , int year);
/*----------------------------------------------------*
    - setOverdraft() is only for Current Account. But as 
        given above, it is an abstract function.
------------------------------------------------------*/
    public abstract boolean setOverdraft(int od);
/*----------------------------------------------------*
    - Withdrawing money has different logic for 
        both type of accounts. Therefore it is an abstract class.
 -----------------------------------------------------*/
    public abstract boolean withdraw(double money);
/*-----------------------------------------------------
    - getType is used for returning which type of account
        the object has.
    - One type(Savings) will return false and 
        one type(Current) will return true.
 ------------------------------------------------------*/
    public abstract boolean getType();
}

//____________________________________________________________________________________________________________________________

/*--------------------------------------------------
    - The class Current is child class of class Account.
---------------------------------------------------*/
class Current extends Account 
{
/*--------------------------------------------------
    - Current class has data member as overdraft.
    - It has data type as integer.
    - Which is used while withdrawing money.
---------------------------------------------------*/

    private int overdarft = 500 ;
    
/*--------------------------------------------------
    Constructor:
        - Passing values are account number(string),
            money(double) and overdraft limit.
        - Account number is passed to the constructor of it's super class.
        - As per company rules, one overdraft limit is 
            ranging from 500 to 10,000,000.
        - This logic is implemented at both main and in the constructor.
---------------------------------------------------*/    
    Current(String acc , double money , int od)
    {
        super(acc);
        amount = money ;
        if (od >= 500 && od <= 10000000)        //if statement is not necessary here.
        {
            overdarft = od ;
        }
    }
    
/*--------------------------------------------------
setOverdraft() function is used for changing value of overdraft limit.
    - If the value passed in the function is in the range of the overdraft, 
        then, it will be passed as data member overdraft and returns true
    - Else it will pass false and value will not change.
---------------------------------------------------*/
    public boolean setOverdraft(int od)
    {
        if (od >= 500 && od <= 10000000) 
        {
            overdarft = od ;
            return true;
        }
        return false;
        
    }

/*-----------------------------------------------------
withdraw() function is used while withdrawing money.
    - Withdraw money cannot be more then the sum of balance available and 
        overdraft limit.
    - If that condition satisfies, than the withdrawn amount will be 
        deducted from the account balance and returns true.
    - Else the function returns false.
-------------------------------------------------------*/
    public boolean withdraw(double money)
    {
        if (money <= amount+overdarft)
        {
            amount -= money ;
            return true;
        }
        return false;
    }
    
/*-----------------------------------------------------------
    getType() function returns true or false value.
    - I have pre-decided that Savings Class will return 
        false and Current will return true
    - Use of it is defined in Savings class.
------------------------------------------------------------*/    
     public boolean getType()
    {
        return true;
    }
     
/*---------------------------------------------------------
addInterst() function is not used in Current type of account.
     therefore, it returns true every time.
---------------------------------------------------------*/     
    public boolean addInterest(int month , int year)
    {
        return true;
    }

/*-------------------------------------------
setDate() and setInterest() is abstract class of Account, but not used here in Current account.
    - therefore it is blank.
----------------------------------------------*/
    public void setDate(int month , int year)
    {}
    
    public void setInterest(float rate){}

}

//____________________________________________________________________________________________________________________________

/*--------------------------------------------------
    - The class Savings is child class of class Account.
---------------------------------------------------*/
class Savings extends Account
{
/*--------------------------------------------------
    - Here data members of the class are
    interest: having data type float. It contains
        the rate of interest in (savings)account.
        The interest is same for all the accounts,
        hence it is taken as static data member.
---------------------------------------------------*/     
    private float interest = 6;
    
/*--------------------------------------------------
    minBal: It has double data type. It is used while
        while withdrawing money.
        - One cannot have balance less than minimum balance.
---------------------------------------------------*/
    private double minBal = 500 ;
/*--------------------------------------------------
    lastMonth, lastYear : It has integer data type.
        - It is used for storing month and year of last
            interest added. As per company rules,
            one(manager) cannot add interest twice in a quarter.
        -For that, this will be used in the function
            addInterest().
---------------------------------------------------*/
    private int lastMonth = 0;
    private int lastYear = 0;

/*--------------------------------------------------
Constructor : 
    - Passing values are AccNumber which is account number,
        which is passed to super constructor(250).
    - One cannot open an account(object) without giving 
        Account Number and deposit money. The class does not
        contain any normal constructor.
    - As per company rules, the deposit money should be 
        greater than or equals to 10,000.
        This logic is applied in main(). But I have also 
        written it in here as well. Which is not necessary.
---------------------------------------------------*/
    Savings (String accNumber , double money)      //constructor
    {
        super(accNumber);
        if (money >= 1000)      //if statement is not necessary
        {
            amount = money ;
        }
    }
    
/*--------------------------------------------------
withdraw() function is used for withdrawing money from 
    particular account.
    - One Cannot withdraw more than money he has and
        the balance in his account should be more or equal to 
        the minimum balance.
    - For that if statement checks whether the deposited amount 
        is exceeding this limit or not. 
    - if the condition goes wrong, the function returns false,
        otherwise, withdrawn money is deducted from account balance 
        and returns true.
---------------------------------------------------*/
    public boolean withdraw(double money)
    {
        if (money <= (amount-minBal))
        {
            amount -= money;
            return true;
        }
        return false;
    }
    
/*--------------------------------------------------
setOverdraft() function takes value (integer) as a parameter
    (same as parent class). 
    -Here this function is not as all used in main().
        Because there nothing like overdraft in Savings account.
---------------------------------------------------*/
    public boolean setOverdraft(int od)
    {
        return false;
    }
    
/*--------------------------------------------------
getType() function returns true or false value.
    - I have pre-decided that Savings Class will return 
        false and Current will return true.
    - This function is helpful in main() for
        checking whether the object created is of Savings 
        class or Current while implementing the concept 
        of polymorphism.
---------------------------------------------------*/    
    public boolean getType()
    {
        return false;
    }
    
/*--------------------------------------------------
addInterest() function takes two parameters, which are
   current month and current year
    - It is used for adding interest to the account.
    - As per company rules, one(manager) can not add interest 
        twice in a quarter. 
    - For that I have two data members in class, lastMonth and
        lastYear, Which tells the last date of the quarter 
        on which interest was added.
    - On the first time, while opening an account, the date on which 
        account created is entered.
    - Here the base of month is 12. therefore we can  write in a single number.
        which is month+(year*12). 
    Logic:
    - (328)If the passed month and year are greater than previous transaction and 
        less than (previous transaction-4), then the interest will be added, and returns true.
    - (340)Otherwise, it will return false.
    - Here, interest is counted using the formula I=P*R*N/100. 
    - (332-336) After adding money to account, it will increment the last month by 4.
        but, the base of month is 12. Hence, if month exceeds 12 then we need to increase 
        year by 1. and reduce the value of month by 1.
------------------------------------------------------*/    
    public boolean addInterest(int month , int year)
    {
        if ((lastMonth + lastYear*12) < (month + year*12) && (lastMonth + lastYear*12 + 3) >= (month + year*12))
        {
            amount += (amount * interest * 0.25 / 100);
            lastMonth += 3;
            if(lastMonth >= 12)
            {
                lastMonth -= 12 ;
                lastYear += 1 ;
            }
            return true ;
        }
        
        return false;
    }
    
/*--------------------------------------------------
setDate() function is used for setting default date 
    for the first time of adding interest.
    - It stores the date creation time in lastMonth and lastYear.
    Logic:
        - It has two parameters which are int. One has value of month and 
            other has year. As per company rules one can add interest once 
            in a quarter. It saves the value as start of quarter.
        - for that, if month is from 1 to 3, then it will save 0.
            if 4 to 6,then it will save 3
            similarly, 7 to 9->6 and 10 to 12->9
        - Year will remain same.
    - It can be implemented in main as well.
---------------------------------------------------*/
    public void setDate(int month , int year)
    {
        int i;
        for(i=0 ; i<4 ; i++)        
        {
            if((month > i*3) && (month <= 3*(i+1)))
                month = i*3;                
        }
        lastMonth = month ;
        lastYear = year ;
    }
    
/*--------------------------------------------------
setInterest() function has float parameter for setting it's 
    value as interest rate. It is used when Bank changes it's
    interest rate.
---------------------------------------------------*/
    public void setInterest(float rate)
    {
        interest =  rate;
    }
}

//____________________________________________________________________________________________________________________________

public class Banking
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);     //Scanner declaration
        int choice,i,count = 0,overdraft,choice1,count1=0;
        int temp , maxAccount, month , year , month1 , year1;
        double deposit;
        String accNum;
        String[] accNumber = new String[100];			//for storing details of account number
		
        boolean check;
        float interest;
        //creating reference of all the classes we need 
        Account object = null;					//Object of account class for banking purpose			
        FileOutputStream fout  = null;			//object of class FileOutputStream and ObjectOutputStream for Serializing an object
        ObjectOutputStream oout = null;			
        FileInputStream fin = null;				//object of class FileInputStream and ObjectInputStream for deserializing the object
        ObjectInputStream oin = null;			
        File f;									//reference of File class for checking existence of the particular account
                        
        /*
            Then it will ask for current month and year which will be passed by function while creating an object.
			It will be needed while adding Interest to all the accounts.
        */
        System.out.print(" Enter Current Month : ");
        month = input.nextInt();
        System.out.print(" Enter Current Year : ");
        year = input.nextInt();
        
        
        while (true)
        {
			//Displaying Menu
            System.out.println("\n****************************************");
            System.out.println(" 1) Create an Account ");
            System.out.println(" 2) Deposit money ");
            System.out.println(" 3) withdraw Money ");
            System.out.println(" 4) Change Overdraft limit ");
            System.out.println(" 5) Check Balance ");
            System.out.println(" 6) Add Interest to all account.");
            System.out.println(" 7) Change Interest rate");
            System.out.println(" 8) Delete an Account ");
            System.out.println(" 9) Exit ");
            System.out.println("****************************************");
            System.out.print(" Enter Your Choice : ");
            choice = input.nextInt();
            switch(choice)
            {
                case 1: //Create an account
                        /*
                        First it will ask for which type of current you want to make. and then it is
                            implemented using switch case.
                        */
                        System.out.println(" 1) Savings Account ");
                        System.out.println(" 2) Current Account ");
                        System.out.print(" Enter Your Choice : ");
                        choice1 = input.nextInt();
                        switch (choice1)
                        {           
                            case 1: //Savings Account creation
                                    System.out.print(" Enter Account Number :");    
                                    accNum = " " ;       // Initialize accNum by a random String (Which solves an error)
                                    accNum = input.next();
                                    
                                    /*
                                    In Savings Account,
                                    The minimum value of deposit should be greater than 10,000
                                    Otherwise it will ask again for giving value of deposit.
                                    */
                                    do
                                    {
                                        System.out.print(" Enter Deposit amount : ");
                                        deposit = input.nextDouble();
                                        if(deposit>=1000)
                                            check = false;
                                        else
                                        {
                                            System.out.println(" Deposit CanNot be less than 1,000 rs. ");
                                            check = true ;
                                        }
                                            
                                    }while(check);
                                    
                                    
                                    //Creating object of Saving class and passing appropriate values to the constructor 
                                    object = new Savings (accNum,deposit);
                                    /*
                                    Passing value of date created (asked in starting)
                                    */
                                    object.setDate(month,year);
									//Success Message
                                    System.out.println(" Account created Successfully.");
                                    try
                                    {
                                        fout = new FileOutputStream(accNum + ".ser"); 	//passing account name to the FileOutputStream to the constructor
                                        oout = new ObjectOutputStream(fout);			//creating object of ObjectInputStream
                                        oout.writeObject(object);						//Writing object data into file
                                        oout.flush();
                                    }
                                    catch(FileNotFoundException ex)			//Catching all the exception generated while serializing object and displaying the Stack Trace
                                    {
                                        ex.printStackTrace();
                                    }
                                    catch(IOException ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                    finally			//closing the file objects and assigning null values to the Objects. So, that they can be used again.
									{
                                        try
                                        {
                                            if(fout!=null)
                                            {
                                                oout.close();
                                                fout.close();
                                                oout = null;
                                                fout = null;
                                            }
                                        }
                                        catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
                                        {
                                            ex.printStackTrace();
                                        }
                                        accNumber[count1] = accNum; 	//Saving Account number to accNumber Array
                                        count1++;
                                    }
                                    break;
                            case 2: //Current Account creation
                                    System.out.print(" Enter Account Number :");
                                    accNum = input.next();
                                    System.out.print(" Enter Deposit amount : ");
									deposit = input.nextLong();
									//deposit cannot be negative in current account.
									// If the entered amount is zero, then it will give message and asks for a proper amount.
									do
									{
										if(deposit >= 0)
											check = false ;     //to stop the while loop
										else
										{
											System.out.println(" Deposit CanNot be negative ");
											check = true;       //to start the while loop again
										}
									}while(check);
									/*
									Range of overdraft limit is from 500 to 10000000.
									This should be implemented in main() only. 
									Otherwise the object will be created if we put this constrain in the class.
									*/
									do
									{
										System.out.print(" Enter overdraft limit : ");
										overdraft = input.nextInt();
										if(overdraft < 500 || overdraft > 10000000)
										{
											System.out.println(" Overdraft should be from 500 to 1 crore.");
											check = true ;      //to stop the while loop
										}
										else
										check = false ;         //to start the while loop again
									}while(check);
								   
									//Creating object of Current class passing appropriate values to the constructor
									object = new Current(accNum , deposit , overdraft);
									/*
                                    Passing value of date created (asked in starting)
                                    */
									object.setDate(month,year);
									//Success Message
									System.out.println(" Account created Successfully.");
									try
									{
										fout = new FileOutputStream(accNum + ".ser");		//passing account name to the FileOutputStream to the constructor
										oout = new ObjectOutputStream(fout);				//creating object of ObjectInputStream
										oout.writeObject(object);							//Writing object data into file
										oout.flush();
									}
									catch(FileNotFoundException ex)							//Catching all the exception generated while serializing object and displaying the Stack Trace
									{
										ex.printStackTrace();
									}
									catch(IOException ex)
									{
										ex.printStackTrace();
									}
									finally					//closing the file objects and assigning null values to the Objects. So, that they can be used again.
									{
										try
										{
											if(fout!=null)
											{
												oout.close();
												fout.close();
												oout = null;
												fout = null;
											}
										}
										catch(IOException ex)		//closing file object also generates IOException, hence catching IOException
										{
											ex.printStackTrace();
										}
									}
									accNumber[count1] = accNum;			//Storing account number to accNumber 
									count1++;
                                    
                                    break;
                            default: System.out.println(" Wrong Choice!!! Please Enter correct choice.");   //default if the input is other then 1 or 2.
                        }
                        break;
                case 2: //Deposit money
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");			//passing account name to the FileInputStream constructor
                            oin = new ObjectInputStream(fin);					//creating object of ObjectOutputStream
                            
                            object = (Account) oin.readObject();				//reading the object data from the file and typecasting it to Account class.
                            System.out.print(" Enter Deposit Amount : ");
                            deposit = input.nextDouble();		//asking for the Deposit amount			
                            if (object.deposite(deposit))       //the deposited amount will be stored in the object we found above.
                                System.out.println(" The amount has deposited successfully.");
                            else            //when the function returns false(in case of negative deposit)
                                System.out.println(" Amount has not deposited.");
                            
                            fout = new FileOutputStream(accNum + ".ser");		//passing account name to the FileOutputStream to the constructor
                            oout = new ObjectOutputStream(fout);				//creating object of ObjectInputStream
                            oout.writeObject(object);							//writing object into file
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)	//Catching all the exception generated while serializing and de-serializing objects and displaying the Stack Trace
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally			//closing the file objects and assigning null values to the Objects. So, that they can be used again.
						{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)		//closing file object also generates IOException, hence catching IOException
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exist.");
                        break;
                case 3: //withdrawing money
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");		//passing account name to the FileInputStream constructor
                            oin = new ObjectInputStream(fin);				//creating object of ObjectOutputStream
                                
                            object = (Account) oin.readObject();			//reading the object data from the file and typecasting it to Account class.
                            System.out.print(" Enter amount to withdraw : ");
                            deposit = input.nextLong();         		    //here I have reused the double deposit
																	   	//otherwise I need to make a new double called withdraw. 
                            if (object.withdraw(deposit))    			  //passing amount to the function
                                System.out.println(" The Amount " + deposit + " has been withdrawn successfully.");     //success message in case of true.
                            else        //unsuccess message in case of false
                                System.out.println(" Withdraw Unsuccessful.");                          
                            fout = new FileOutputStream(accNum + ".ser");	//passing account name to the FileOutputStream to the constructor
                            oout = new ObjectOutputStream(fout);			//creating object of ObjectInputStream
                            oout.writeObject(object);						//writing object into file
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)	//Catching all the exception generated while serializing and de-serializing objects and displaying the Stack Trace
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally		//closing the file objects and assigning null values to the Objects. So, that they can be used again.
						{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)		//closing file object also generates IOException, hence catching IOException
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exists.");
                        
                        break;
                case 4: //change overdraft limit
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
                        try
                        {
                            fin = new FileInputStream(accNum + ".ser");			//passing account name to the FileInputStream constructor
                            oin = new ObjectInputStream(fin);					//creating object of ObjectOutputStream
                            object = (Account) oin.readObject();				//reading the object data from the file and typecasting it to Account class.
                            System.out.print(" Enter new overdraft limit : ");
							overdraft = input.nextInt();						//asking for new overdraft value
							//Passing the value to the setOverdraft and overdraft is a boolean function 
							if (object.setOverdraft(overdraft))			
								System.out.println("Overdraft changed Successfully.");
							else
								System.out.println(" Overdraft limit is out of range.");
											   
                            fout = new FileOutputStream(accNum + ".ser");		//passing account name to the FileOutputStream to the constructor
                            oout = new ObjectOutputStream(fout);				//creating object of ObjectInputStream
                            oout.writeObject(object);							//writing object into file
                            oout.flush();
                        }
                        catch(ClassNotFoundException ex)	//Catching all the exception generated while serializing and de-serializing objects and displaying the Stack Trace
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally			//closing the file objects and assigning null values to the Objects. So, that they can be used again.
						{
                            try
                            {
                                if(fout!=null)
                                {
                                    oin.close();
                                    fin.close();
                                    oin = null;
                                    fin = null;
                                    oout.close();
                                    fout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exist.");
                        break;
                case 5: //check Balance
                        System.out.print(" Enter Account Number : ");
                        accNum = input.next();
                        f = new File(accNum + ".ser");
                        if(f.exists())
                        {
							try
							{
								fin = new FileInputStream(accNum + ".ser");		//passing account name to the FileInputStream constructor
								oin = new ObjectInputStream(fin);				//creating object of ObjectOutputStream
								object = (Account) oin.readObject();			//reading the object data from the file and typecasting it to Account class.			
								System.out.print(" Balance in the account is : " + object.getAmount());
								  //Printing balance of account using getAmount function
							}
							catch(ClassNotFoundException ex)	//Catching all the exception generated while de-serializing objects and displaying the Stack Trace
							{
								ex.printStackTrace();
							}
							catch(FileNotFoundException ex)
							{
								ex.printStackTrace();
							}
							catch(IOException ex)
							{
								ex.printStackTrace();
							}
							finally	//closing the file objects and assigning null values to the Objects. So, that they can be used again.
							{
								try
								{
									oin.close();
									fin.close();
								}
								catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
								{
									ex.printStackTrace();
								}
								
								oin = null;
								fin = null;
							}
                        }
                        else
                            System.out.println("Account does not exist.");
                        
                        break;
                case 6: //Add Interest to all account
						System.out.print(" Enter Current Month : ");		//asking for current month and year for adding interest
                        month1 = input.nextInt();
                        System.out.print(" Enter Current Year : ");
                        year1 = input.nextInt();
                        check = false;				
						/*
                        pre-set value of check is false, if the last account in savings returns true, then it shows that interest has been added,
                            otherwise, the interest has not been added.
                        */						
                        for(i=0 ; i < accNumber.length ; i++)
                        {
							f = new File(accNumber[i] + ".ser");
							if(f.exists())
							{
								try
								{
									fin = new FileInputStream(accNumber[i] + ".ser");			//passing account name to the FileInputStream constructor
									oin = new ObjectInputStream(fin);							//creating object of ObjectOutputStream
									object = (Account) oin.readObject();						//reading the object data from the file and typecasting it to Account class.
									/*
										getType returns false if the account is of Savings type and true for current type
										So, we will add interest to only to the Savings type account
										Even we have write nothing to the addInterest function in Current Class.
									*/
									if(!object.getType())      //adding interest to every account.
										check = object.addInterest(month1, year1);
														 
									fout = new FileOutputStream(accNumber[i] + ".ser");			//passing account name to the FileOutputStream to the constructor
									oout = new ObjectOutputStream(fout);						//creating object of ObjectInputStream
									oout.writeObject(object);									//writing object into file
									oout.flush();
								}
								catch(ClassNotFoundException ex)	//Catching all the exception generated while serializing and de-serializing objects and displaying the Stack Trace
								{
									ex.printStackTrace();
								}
								catch(FileNotFoundException ex)
								{
									ex.printStackTrace();
								}
								catch(IOException ex)
								{
									ex.printStackTrace();
								}
								finally
								{
									try
									{
										if(fout!=null)	//closing the file objects and assigning null values to the Objects. So, that they can be used again.
										{
											oin.close();
											fin.close();
											oin = null;
											fin = null;
											oout.close();
											fout.close();
											oout = null;
											fout = null;
										}
									}
									catch(IOException ex)	//closing file object also generates IOException, hence catching IOException
									{
										ex.printStackTrace();
									}
								}
							}
                        }
                        if(check)
                            System.out.println(" The Interest has been added to every account.");
                        else
                            System.out.println(" The Interest has not been added.");
                    
                    
                    
                    
                        break;
                case 7: //setting new interest rate.
						System.out.print(" Enter new Interest rate : ");
                        interest = input.nextFloat();		//asking for new interest rate.
						/*
                        The default value of interest is 6,
                        the entered amount is passed to the function setInterest() function.
                        */
                        for(i=0 ; i < accNumber.length ; i++)		//using for loop to change interest rate to all the accounts.
                        {
							f = new File(accNumber[i] + ".ser");	
							if(f.exists())
							{
								try
								{
									//de-serializing the object
									fin = new FileInputStream(accNumber[i] + ".ser");	//passing account name to the FileInputStream constructor
									oin = new ObjectInputStream(fin);					//creating object of ObjectOutputStream	
									object = (Account) oin.readObject();				//reading the object data from the file and typecasting it to Account class.
									object.setInterest(interest);						//Setting Interest to the account.
									//serializing the object				 
									fout = new FileOutputStream(accNumber[i] + ".ser");	//passing account name to the FileOutputStream to the constructor
									oout = new ObjectOutputStream(fout);				//creating object of ObjectInputStream
									oout.writeObject(object);							//writing object into file
									oout.flush();
								}
								catch(ClassNotFoundException ex)		//Catching all the exception generated while serializing and de-serializing objects and displaying the Stack Trace
								{
									ex.printStackTrace();
								}	
								catch(FileNotFoundException ex)
								{
									ex.printStackTrace();
								}
								catch(IOException ex)
								{
									ex.printStackTrace();
								}
								finally
								{
									try
									{
										if(fout!=null)			//closing the file objects and assigning null values to the Objects. So, that they can be used again. 
										{
											oin.close();
											fin.close();
											oin = null;
											fin = null;
											oout.close();
											fout.close();
											oout = null;
											fout = null;
										}
									}
									catch(IOException ex)		//closing file object also generates IOException, hence catching IOException
									{
										ex.printStackTrace();
									}
								}							
							}
                        }
                        System.out.println(" The interest rate has been changed successfully.");	//success message
                        break;
                    
                        
                case 8: //deleting an account
						System.out.print(" Enter Account Number you want to delete : ");
                        accNum = input.next();		//asking for account number 
                        f = new File(accNum + ".ser");		//creating an object of file by passing he file name as a parameter in constructor
                        if(f.exists())
                        {
                            f.delete();						//Deleting the account of the file Exist.
                            System.out.println(" The Account deleted Successfully.");		//Printing success message
                        }
                        else		//Statement will be printed when the account does not exist.
                            System.out.println(" Account does not exist.");
                        break;
                case 9: System.exit(0); //exit
                default:System.out.println(" Wrong Choice!!! Please Enter correct choice.");
            }
        }   
    }      
}

