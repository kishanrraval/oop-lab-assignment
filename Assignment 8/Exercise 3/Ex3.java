import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;			//Importing classes which are needed.
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;


class Engine implements Serializable    //Class for engine which has one to one relationship with class Vehicle which has licence for serialization 
{
	private int horsePower;              // Data member for saving horsePower of engine
    private String company;             // for saving company name of manufacturer
    
    Engine(int hp, String name)        //Constructor having two parameters
    {                                   //hp is passing value to horsePower and name is passing value to company
        this.horsePower = hp;
        this.company = name;
    }
    
    public int getHorsePower()          //returning value of horsePower which is integer
    {
        return horsePower;
    }
    public String getCompany()          //returning the value of manufacturer company which is string
    {
        return company;
    }
    
    public void setHorsePower(int hp)       //Setting new value to horsePower which will be used for updation of values. 
    {
        horsePower = hp;
    }
    public void setCompany(String name)     //Setting new value to company which will be used for updation of values.
    {
        company = name;
    }
}
/*
* Vehicle class is a super class of FourWheeler and TwoWheeler class and it is used in main for 
      implementing polymorphism
*/
abstract class Vehicle implements Serializable              
{       //all this variables will be used for display and edition purpose in the child classes, so they are protectd.
    protected String name;      //Used for saving name of the vehicle
    protected String plateNo;   //for saving number plate (registration number)
    protected double cost;      //for saving price of the vehicle
    //Mileage, weight and speed are taken int because there can be a use to make an ascending or descending list using them or thers.
    protected int milage;       //for saving mileage of vehicle
    protected int weight;       // for saving weight of vehicle
    protected int speed;        // for saving speed of the vehicle.
    Engine eng;                 //created reference of the Engine for the container-ship relation
    
    Vehicle(String name, String number, double cost, int milage, int weight, int speed, int hpower, String company)     //constructor
    {           //passing every value to it.
        this.name = name;           
        this.plateNo = number;
        this.cost = cost;
        this.milage = milage;
        this.weight = weight;
        this.speed = speed;
        eng = new Engine(hpower, company);      //creating instance and passing value of horsePower to the constructor
    }
    
    //Making same function as in engine class for the use of the child class for displaying and editing.
    public int getHorsePower()             
    {
        return eng.getHorsePower();     //returning the returned value from getHorsePower() function of Engine class.
    }
    public String getCompany()
    {
        return eng.getCompany();
    }
    
    public void setHorsePower(int hp)       //setting a new value of horsePower 
    {
        eng.setHorsePower(hp);
    }
    public void setCompany(String name)     //setting a new value of company of engine
    {
        eng.setCompany(name);
    }
    
    abstract void display();                
//abstract function declaration of displaying value. It will be helpful for using reference of Vehicle class for using objects of sub classs using polimorphism
    abstract void update(int choice);
//similarly function for updation of values.    
}


class TwoWheeler extends Vehicle        // TwoWheeler class for making object of twoWheeler
{
    private boolean spareWheel;         //true-> if the vehicle has a spare wheel otherwise false.
    private boolean gear;               //similarly for gear and frontDecky
    private boolean frontDecky;
    
    //constructor having all the values of data members as parameters
    TwoWheeler(boolean sw, boolean gear, boolean Decky, String name, String number, double cost, int milage, int weight, int speed, int hpower, String company)     //Constructor
    {
        super(name,number,cost,milage,weight,speed, hpower, company);       //passing value to the super class constructor
        spareWheel = sw;                //assigning values to data members
        this.gear = gear;
        frontDecky = Decky;
    }
    public void display()       //displaying the values of data-members
    {
        System.out.println("                    Two Wheeler");
        System.out.println("_______________________________________________________");
        System.out.println("1) Name         : " + this.name);
        System.out.println("2) Vehicle Num. : " + this.plateNo);
        System.out.println("3) Cost         : " + this.cost);
        System.out.println("4) Mileage       : " + this.milage);
        System.out.println("5) Weight       : " + this.weight);
        System.out.println("6) Speed        : " + this.speed);
        System.out.println("7) Company Name : " + this.getCompany());
        System.out.println("8) Horse Power  : " + this.getHorsePower()); 
        System.out.print("9) Spare-wheel   : " );        //spare-wheel,gear and frontDecky are boolean. printing yes if value is true, no if false
        if(this.spareWheel == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.print("10) Gear        : " );
        if(this.gear == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.print("11) Front Decky : " );
        if(this.frontDecky == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.println("_______________________________________________________");
    }
    
    public void update(int choice)          //function for updation of values
    {
        Scanner input = new Scanner(System.in);
        int integer;            
        String string;
        double dbl;
        this.display();
        
        System.out.print(" Enter Updated Value : ");
        switch(choice)      //choice is the number user wants to update printed in display.
       {                    //Taking new value from user of choice number which he entered.
           case 1:  string = input.next();
                    this.name = string;
                    break;
           case 2:  string = input.next();
                    this.plateNo = string;
                    break;
           case 3:  dbl = input.nextDouble();
                    this.cost = dbl;
                    break;
           case 4:  integer = input.nextInt();
                    this.milage = integer;
                    break;
           case 5:  integer = input.nextInt();
                    this.weight = integer;
                    break;
           case 6:  integer = input.nextInt();
                    this.speed = integer;
                    break;
           case 7:  string = input.next();
                    this.setCompany(string);
                    break;
           case 8:  integer = input.nextInt();
                    this.setHorsePower(integer);
                    break;
           case 9:  System.out.print("\nPress 1 for yes, 0 for No : ");     //1 for true and 0 for false
                    integer = input.nextInt();                    //if integer is 1 then the valiu will be true else it will false.
                    if(integer == 1)
                        this.spareWheel = true;
                    else
                        this.spareWheel = false;
                    break;
           case 10: System.out.print("\nPress 1 for yes, 0 for No : ");     
                    integer = input.nextInt();
                    if(integer == 1)
                        this.gear = true;
                    else
                        this.gear = false;
                    break;
           case 11: System.out.print("\nPress 1 for yes, 0 for No : ");
                    integer = input.nextInt();
                    if(integer == 1)
                        this.frontDecky = true;
                    else
                        this.frontDecky = false;
                    break;
           default:System.out.println(" Wrong Choice!!! Please enter aa correct choice.");
       }
    }
}

abstract class FourWheeler extends Vehicle          //FourWheeler class which is parent class of PrivateCar and PublicCar and child class of Vehicle
{
    protected String type;                          //storing type of car eg. shedan, XUV,...
    protected boolean audioSystem;                  //true if the car has audio system else false
    protected boolean powerStearing;                //true if the car has power stearing else false.
            //constructor and passing values of data-members as parameters.
    FourWheeler(String type, boolean audioSystem, boolean powerStearing, String name, String number, double cost, int milage, int weight, int speed, int hpower, String company)
    {
        super(name, number, cost, milage, weight, speed, hpower, company);      //passing values to super class constructor
        this.type = type;                                                       //setting values coming from constructor
        this.audioSystem = audioSystem;
        this.powerStearing = powerStearing;
    }
    
    abstract void display();                                                    
//abstract display() is defined because it is an abstract function of super class. it will be used in the child class.
}

class PrivateCar extends FourWheeler        //Class FourWheeler is child class of FourWheler. 
{
    private int capesity;                   //one more data-member which is not in the parent class.
            //constructor and passing values of data-members as parameters.
    PrivateCar(int capesity, String type, boolean audioSystem, boolean powerStearing, String name, String number, double cost, int milage, int weight, int speed, int hpower, String company)
    {
        super(type, audioSystem, powerStearing, name, number, cost, milage, weight, speed, hpower, company);        //passing values to the super class constructor
        this.capesity = capesity;
    }
    
    public void display()               //function for displaying value of data members of the class and parent class as well. 
    {
        System.out.println("                    Private Car");
        System.out.println("_______________________________________________________");
        System.out.println("1) Name         : " + this.name);
        System.out.println("2) Vehicle Num. : " + this.plateNo);
        System.out.println("3) Cost         : " + this.cost);
        System.out.println("4) Mileage       : " + this.milage);
        System.out.println("5) Weight       : " + this.weight);
        System.out.println("6) Speed        : " + this.speed);
        System.out.println("7) Company Name : " + this.getCompany());
        System.out.println("8) Horse Power  : " + this.getHorsePower());
        System.out.println("9) Type         : " + this.type);
        System.out.print("10) Audio System : ");
        if(this.audioSystem == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.print("11) PowerStearing:");
        if(this.powerStearing == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.println("12) Capecity     : " + this.capesity);
        System.out.println("_______________________________________________________");
        
    }
    //function for updating values of data members.
    public void update(int choice)
    {
        Scanner input = new Scanner(System.in);
        int integer;
        String string;
        double dbl;
        System.out.print(" Enter Updated Value : ");            //asking for the new Value and Scanner is inside thr case.
        switch(choice)
        {                                                   //cases are made as numbers printed in the display and in the main I have called the display() before this function
           case 1:  string = input.next();                  //taking appropriate input from user 
                    this.name = string;                     //storing the value of input to the data members. 
                    break;
           case 2:  string = input.next();
                    this.plateNo = string;
                    break;
           case 3:  dbl = input.nextDouble();
                    this.cost = dbl;
                    break;
           case 4:  integer = input.nextInt();
                    this.milage = integer;
                    break;
           case 5:  integer = input.nextInt();
                    this.weight = integer;
                    break;
           case 6:  integer = input.nextInt();
                    this.speed = integer;
                    break;
           case 7:  string = input.next();
                    this.setCompany(string);
                    break;
           case 8:  integer = input.nextInt();
                    this.setHorsePower(integer);
                    break;
           case 9:  string = input.next();
                    this.type = string;
                    break;
           case 10: System.out.print("\nPress 1 for yes, 0 for No : ");         //boolean might not be familiar to the user hense I have useed 0 and 1
                   integer = input.nextInt();
                    if(integer == 1)
                        this.audioSystem = true;            //storing value as per input.
                    else
                        this.audioSystem = false;
                    break;
           case 11: System.out.print("\nPress 1 for yes, 0 for No : ");
                    integer = input.nextInt();
                    if(integer == 1)
                        this.powerStearing = true;
                    else
                        this.powerStearing = false;
                    break;
           case 12: integer = input.nextInt();
                    this.capesity = integer;
                    break;
           default:System.out.println(" Wrong Choice!!! Please enter a correct choice.");
       }
    }
}

class CommercialCar extends FourWheeler             //Commercial class is child case of FourWheeler class
{                                                   // two more data members which are not in super class.
    private boolean transportation;                 //true if the vehicle is used for transportation else false 
    private int capasity;                           //capacity if transportation is true than unit will be in KG.
                                                            //if transportation is false than unit will be number of person
    //constructor and passing value of all the data members as parameters.
    CommercialCar(boolean transportation, int capasity, String type, boolean audioSystem, boolean powerStearing, String name, String number, double cost, int milage, int weight, int speed, int hpower, String company)
    {                       //passing values to the super class constructor.
        super(type, audioSystem, powerStearing, name, number, cost, milage, weight, speed, hpower, company);
        this.transportation = transportation;
        this.capasity = capasity;
    }
    public void display()                   
    {
        System.out.println("                    Commercial Car");
        System.out.println("_______________________________________________________");
        System.out.println("1) Name          : " + this.name);
        System.out.println("2) Vehicle Num.  : " + this.plateNo);
        System.out.println("3) Cost          : " + this.cost);
        System.out.println("4) Mileage        : " + this.milage);
        System.out.println("5) Weight        : " + this.weight);
        System.out.println("6) Speed         : " + this.speed);
        System.out.println("7) Company Name  : " + this.getCompany());
        System.out.println("8) Horse Power   : " + this.getHorsePower());
        System.out.println("9) Type          : " + this.type);
        System.out.print("10) Audio System : ");
        if(this.audioSystem == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.print("11) PowerStearing:");
        if(this.powerStearing == true)
            System.out.println(" Yes");
        else
            System.out.println(" No");
        System.out.print("12) Transportation: ");
        if(this.transportation == true)
        {    System.out.println(" Yes");
            System.out.println("13) Capacity     : " + this.capasity + " KG");
        }
        else
        {
            System.out.println(" No");
            System.out.println("13) Capacity     : " + this.capasity + " Person");
        }
        System.out.println("_______________________________________________________"); 
    }
    //function for updating values of data members.    
    public void update(int choice)
    {
        Scanner input = new Scanner(System.in);
        int integer;
        String string;
        double dbl;
        System.out.print(" Enter Updated Value : ");            //asking for the new Value and Scanner is inside the case.
        switch(choice)                                          //cases are made as numbers printed in the display and in the main I have called the display() before this function
        {
           case 1:  string = input.next();                      //taking appropriate input from user 
                    this.name = string;                         //storing values to the data members
                    break;
           case 2:  string = input.next();
                    this.plateNo = string;
                    break;
           case 3:  dbl = input.nextDouble();
                    this.cost = dbl;
                    break;
           case 4:  integer = input.nextInt();
                    this.milage = integer;
                    break;
           case 5:  integer = input.nextInt();
                    this.weight = integer;
                    break;
           case 6:  integer = input.nextInt();
                    this.speed = integer;
                    break;
           case 7:  string = input.next();
                    this.setCompany(string);
                    break;
           case 8:  integer = input.nextInt();
                    this.setHorsePower(integer);
                    break;
           case 9:  string = input.next();
                    this.type = string;
                    break;
           case 10: System.out.print("\nPress 1 for yes, 0 for No : ");         //boolean might not be familiar to the user hence I have used 0 and 1
                    integer = input.nextInt();
                    if(integer == 1)
                        this.audioSystem = true;                                //storing values as per input
                    else
                        this.audioSystem = false;
                    break;
           case 11: System.out.print("\nPress 1 for yes, 0 for No : ");
                    integer = input.nextInt();
                    if(integer == 1)
                        this.powerStearing = true;
                    else
                        this.powerStearing = false;
                    break;
           case 12: System.out.print("\nPress 1 for yes, 0 for No : ");
                    integer = input.nextInt();
                    if(integer == 1)
                        this.transportation = true;
                    else
                        this.transportation = false;
                    System.out.println("\n Enter new Value for Capacity.");
           case 13: integer = input.nextInt();
                    this.capasity = input.nextInt();
                    break;
           default:System.out.println(" Wrong Choice!!! Please enter a correct choice.");
       }
    }
}

public class Ex3 
{
    public static void main(String[] args) 
    {
        Scanner input= new Scanner(System.in);
        int choice,choice1,choice2,milage,weight,speed,capesity,hp;
        String name=" ",plateNo=" ",type=" ",company=" ";               //assigning random value which solves an error.
        double cost;
        byte temp;
        boolean spareWheel,gear,frontDecky,audio,powerStearing,transportation;
        Vehicle object = null;  
        FileOutputStream fout  = null;
        ObjectOutputStream oout = null;
        FileInputStream fin = null;
        ObjectInputStream oin = null;

        while(true)
        {
            File f = null;
            System.out.println("****************************************");         //displaying menu
            System.out.println(" 1) Add a new record ");
            System.out.println(" 2) Update a record");
            System.out.println(" 3) Delete a record");
            System.out.println(" 4) View a record");
            System.out.println(" 5) Exit");
            System.out.println("****************************************");
            System.out.print(" Enter Your Choice : ");
            choice = input.nextInt();
            
            switch(choice)
            {
                case 1: //Add a new record
                        System.out.println(" 1) Two Wheeler ");         //asking for the type of new record
                        System.out.println(" 2) Private Car");
                        System.out.println(" 3) Public Car ");
                        System.out.print(" Enter Your Choice : ");    
                        choice1 = input.nextInt();
                        System.out.print(" Enter Name : ");             //asking the common entities before switch case
                        name = input.next();
                        System.out.print(" Enter Plate Number : ");
                        plateNo = input.next();
                        System.out.print(" Enter Cost : ");
                        cost = input.nextDouble();
                        System.out.print(" Enter Mileage : ");
                        milage = input.nextInt();
                        System.out.print(" Enter Speed : ");
                        speed = input.nextInt();
                        System.out.print(" Enter Weight :");
                        weight = input.nextInt();
                        System.out.print(" Enter Company of Engine : ");
                        company = input.next();
                        System.out.print(" Enter horse Power of engine : ");
                        hp = input.nextInt();
                        switch(choice1)                         //asking values as per type of record
                        {
                            case 1: //Two Wheeler
                                    System.out.print(" Enter  1 if it has a spare-wheel else enter 0 : ");
                                    temp = input.nextByte();
                                    spareWheel = false;         //preset value is false 
                                    if(temp == 1)               //if the input is 1, then value will be stored as true.
                                        spareWheel = true;
                                    System.out.print(" Enter 1 if it has gear else enter 0 : ");
                                    temp  = input.nextByte();
                                    gear = false;
                                    if(temp == 1)
                                        gear = true;
                                    System.out.print(" Enter 1 if it has front Decky else enter 0 : ");
                                    temp  = input.nextByte();
                                    frontDecky = false;         
                                    if(temp == 1)
                                        frontDecky = true;
                                    //creating object of TwoWheeler class and passing all the values to the constructor
                                    object = new TwoWheeler(spareWheel, gear, frontDecky, name, plateNo, cost, milage, weight, speed, hp, company);
                                    System.out.println(" Record created Successfully.");        //success message
                                    
                                    try          //serializing object and give the file name same as name of the record
                                    {
                                        fout = new FileOutputStream(name + ".ser");           //creating object of FileOutputStream 
                                        oout = new ObjectOutputStream(fout);                  //creating object of ObjectOutputStream
                                        oout.writeObject(object);                             //serializing the object
                                        oout.flush();                                                                               
                                    }
                                    catch(FileNotFoundException ex)         //catching the File not found exception 
                                    {
                                        ex.printStackTrace();               //printing the details
                                    }
                                    catch(IOException ex)                   //catching IOException
                                    {
                                        ex.printStackTrace();               //printing details
                                    }
                                    finally{                                //closing the objects of outputStream and putting null value
                                        try 
                                        {
                                            if(fout!=null)
                                            {
                                                oout.close();
                                                fout.close();
                                                oout = null;
                                                fout = null;
                                            }
                                        }
                                        catch(IOException ex)           //catching IOException of try block and printing the details.
                                        {
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;
                            case 2: //private car
                                    System.out.print(" Enter type : ");         //getting values which are not taken before the switch case
                                    type = input.next();
                                    System.out.print(" Enter 1 if it has Audio System else enter 0 : ");
                                    temp  = input.nextByte();
                                    audio = false;
                                    if(temp == 1)
                                        audio = true;
                                    System.out.print(" Enter 1 if has Power Stearing else enter 0 : ");
                                    temp  = input.nextByte();
                                    powerStearing = false;
                                    if(temp == 1)
                                        powerStearing = true;
                                    System.out.print(" Enter Capesity : ");
                                    capesity = input.nextInt();
                                    //creating a object of class PrivateCar and passing all the values to the constructor
                                    object = new PrivateCar(capesity, type, audio, powerStearing, name, plateNo, cost, milage, weight, speed, hp, company);
                                    System.out.println(" Record created Successfully.");        //succes message
                                    try                  //serializing object and give the file name same as name of the record
                                    {
                                        fout = new FileOutputStream(name + ".ser");             //creating object of FileOutputStream
                                        oout = new ObjectOutputStream(fout);                    //creating object of ObjectOutputStream
                                        oout.writeObject(object);                               //serializing the object
                                        oout.flush();
                                       
                                    }
                                    catch(FileNotFoundException ex)                             //catching the File not found exception
                                    {
                                        ex.printStackTrace();                                   //printing the details
                                    }
                                    catch(IOException ex)                                       //catching IOException
                                    {
                                        ex.printStackTrace();                                   //printing the details
                                    }
                                    finally{
                                        try
                                        {
                                            if(fout!=null)                  //closing the objects of outputStream and putting null value
                                            {
                                                oout.close();
                                                fout.close();
                                                oout = null;
                                                fout = null;
                                            }
                                        }
                                        catch(IOException ex)               //catching IOException of try block and printing the details.
                                        {
                                            ex.printStackTrace();
                                        }
                                    }
                        
                                    break;    
                            case 3: //Commercial car
                                    System.out.print(" Enter type : ");         //getting values which are not taken before the switch case
                                    type = input.next();
                                    System.out.print(" Enter 1 if it has Audio System else enter 0 : ");
                                    temp  = input.nextByte();
                                    audio = false;
                                    if(temp == 1)
                                        audio = true;
                                    System.out.print(" Enter 1 if has Power Stearing else enter 0 : ");
                                    temp  = input.nextByte();
                                    powerStearing = false;
                                    if(temp == 1)
                                        powerStearing = true;
                                    System.out.print(" Enter 1 if it is used for transportation else enter 0 : ");
                                    temp  = input.nextByte();
                                    transportation = false;
                                    if(temp == 1)
                                        transportation = true;
                                    System.out.print(" Enter Capesity in ");
                                    if(transportation)
                                       System.out.print("KG : ");
                                    else
                                        System.out.print("number of person :");
                                    capesity = input.nextInt();
                                    //creating a object of class CommercialCar and passing all the values to the constructor
                                    object = new CommercialCar(transportation, capesity, type, audio, powerStearing, name, plateNo, cost, milage, weight, speed, hp, company);
                                    System.out.println(" The record created Successfully.");        //success message
                                    try                             
                                    {           //serializing object and give the file name same as name of the record
                                        fout = new FileOutputStream(name + ".ser");                 //creating object of FileOutputStream
                                        oout = new ObjectOutputStream(fout);                        //creating object of ObjectOutputStream
                                        oout.writeObject(object);                                   //serializing the object
                                        oout.flush();
                                    }
                                    catch(FileNotFoundException ex)                                 //catching the File not found exception
                                    {
                                        ex.printStackTrace();                                       //printing the details
                                    }
                                    catch(IOException ex)                                           //catching IOException
                                    {
                                        ex.printStackTrace();                                       //printing the details
                                    }
                                    finally{
                                        try
                                        {
                                            if(fout!=null)                                  //closing the objects of outputStream and putting null value
                                            {
                                                oout.flush();
                                                oout.close();
                                                fout.close();
                                                oout = null;
                                                fout = null;
                                            }
                                        }
                                        catch(IOException ex)                               //catching IOException of try block and printing the details.
                                        {
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;
                            
                            default: System.out.println(" Wrong Choice!!! Please Enter a proper choice.");
                        }
						System.out.println("The Record is saved by the name " +name +" \n It will be used in future.");
                        break;
                case 2: //Update a record 
                        System.out.print(" Enter Record name you want to update : ");
                        name = input.next();          //we have serialized the object as name of record                                  
                                //asking for name f record and using it for opening that file and de serializing it
                        f = new File(name + ".ser");
                        if(f.exists())                  //de serializing if file exists else printing that record does not exists
                        {
                        
                        try
                        {
                            fin = new FileInputStream(name + ".ser");
                            oin = new ObjectInputStream(fin);
                            object = (Vehicle) oin.readObject();            //getting Input from file and typecasting it to the object of class Vehicle
                            object.display();                   //displaying value and getting number which user wants to update
                            System.out.print("Enter number you want to update : ");
                            choice2 = input.nextInt();              
                            object.update(choice2);             //passing the number to the update() function
                            System.out.println(" The Value has been updated successfully.");
                            fout = new FileOutputStream(name + ".ser");         //serializing the object 
                            oout = new ObjectOutputStream(fout);
                            oout.writeObject(object);
                            
                            oout.flush();
                            
                        }
                        catch(ClassNotFoundException ex)            //catching exceptions
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try
                            {
                                if(fout!=null)                  //closing files and giving null values to objects of input and output stream  
                                {   
                                    fin.close();
                                    oin.close();
                                    oin = null;
                                    fin = null;
                                    fout.close();
                                    oout.close();
                                    oout = null;
                                    fout = null;
                                }
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else 
                           System.out.println(" File does not exist.");
                        
                        break;
                case 3: //deleting a record
                        System.out.print(" Enter Record name you want to delete : ");
                        name = input.next();
                        f = new File(name + ".ser");        //taking name and passing value to the constructor of File class
                       
                        if(f.exists())
                        {
                            f.delete();             //deleting the file
                            System.out.println(" The Record deleted Successfully.");            //success message
                        }
                        else
                            System.out.println(" Record does not exist.");      //printing when file does not exist.
                        break;
                case 4: //displaying value of record
                        System.out.print(" Enter record name : ");
                        name = input.next();
                        f = new File(name + ".ser");        //getting name of file 
                        if(f.exists())
                        {
                        
                        try
                        {
                            fin = new FileInputStream(name + ".ser");               //de serializing the object
                            oin = new ObjectInputStream(fin);
                            object = (Vehicle) oin.readObject();                    //getting input from file and typecasting it to the object
                            object.display();                                       //displaying the file
                             
                        }
                        catch(ClassNotFoundException ex)            //catching exceptions
                        {
                            ex.printStackTrace();
                        }
                        catch(FileNotFoundException ex)
                        {
                            ex.printStackTrace();
                        }
                        catch(IOException ex)
                        {
                            ex.printStackTrace();
                        }
                        finally{
                            try{
                                oin.close();            //closing files and giving null values to objects of input stream 
                                fin.close();
                                oin = null;
                                fin = null;
                            }
                            catch(IOException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        }
                        else
                            System.out.println("Account does not exist.");          //printing when file does not exist.
                        break;
                case 5: System.exit(0);             //exit from the program
                default: System.out.println(" Wrong Choice!!! Please enter a proper choice.");
            }
        }
    } 
}