#include<iostream>
#include<cstdlib>
using namespace std;

class  Stack            //class for stack implementation
{
private:
    struct node
    {
        int data;           //for storing data of a node
        node *next;         //for storing address of next node
    };
    node *top;              //for storing adress of top node
public:


    node *p;
    Stack();                    //constructor
    ~Stack();                   //destructor
    void push(int item);        //function for push
    int pop();                  //function for pop
    int peek();                 //function for peek which shows the highest value
    void display();             //function for displaying the list
    int isempty();              //function for checking whether stack is empty or not
};

Stack::Stack()
{
    top=NULL;       //constructor
}
Stack::~Stack;()
{
    //destructor
}
void Stack::push(int item)
{
    p=new node;
    p->data=item;               //push
    p->next=top;
    top=p;
}
int Stack::pop()
{
    if (isempty())
    {
        cout << "The Stack is empty" << endl;
        return NULL;
    }
    node *temp=top;                 //pop
    top=temp->next;
    return temp->data;
}
int Stack::peek()
{
    if (isempty())
    {
        cout << "The Stack is empty" << endl;
        return NULL;
    }
    return top->data;
}
void Stack::display()
{
    if(top==NULL)               //displaying the list
    {
        cout << "The list is empty";
    }
    p=top;
    while(p != NULL)
    {
        cout << "\n " << p->data;
        p=p->next;
    }
}
int Stack::isempty()
{
    return top == NULL;                 //checking whether stack is empty or not
}


int main()
{
    Stack obj;
    again:while(1)
    {
    int choice,popNo;
    //main menu
    cout << "\n1) Push \n2) Pop \n3) Peek \n4) Display \n5) Exit\n Enter Your choice:";
    cin>>choice;
    switch (choice)
    {
        case 1: cout << "Enter a number you want to push:";
                cin>>popNo;
                obj.push(popNo);break;              //push
        case 2: cout << "\nThe number " << obj.pop() << " has been deleted from the stack\n\n"; break;  //pop
        case 3: cout << "\n The peek number is " << obj.peek(); break;      //peek
        case 4: cout << "List:\n";          //displaying the dist
                obj.display(); break;
        case 5:goto end;                //nding the program
        default: cout << "\nWrong Choice!! Plaease Enter proper choice.";               //error handling
                    goto again; break;
    }}

    end:return 0;
}
