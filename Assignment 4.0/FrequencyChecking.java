import static java.lang.System.exit;
  import java.util.*;

public class FrequencyChecking {
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        int a,b,i,count=0,temp = 0,max=0,maxNo=0;       //a for starting number; b for ending number; count[10] for counting the frqncy max is the most number of frquncy
        char[] ans = new char[30000];                   // maxNo for the number which has highest frqncy
        int[] frq = new int[10];
        for(i=0;i<10;i++)
        {
            frq[i] = 0 ;                //putting initial value of frequency to 0
        }
        System.out.print("Enter Value of A:");
        a = input.nextInt();                            // Getting A
        System.out.print("Enter Value of B:");
        b = input.nextInt();                            // Getting B
        if(a>b)
        {
            System.out.println("Value of A is bigger than value of B");         //Error handling(if b > a ) then program will stop excecuting 
            exit (0);
        }
        for(i=a;i<=b;i++)               //for putting numer into a char array using modulas and division method
        {
           if(i<10 && i>=0)
           {
               ans[count]=(char) i;         //for 1 size of number 
               count++;
           }
           else if(i>9 && i<100)
           {
               temp = i/10;                 //for 2 size f number
               ans[count] = (char) temp;
               count++;
               temp = i%10;
               ans[count] = (char) temp;
               count++;
           }
           else if(i>99 && i<1000)
           {
               temp = i/100;                // for 3 size of number
               ans[count] = (char) temp;
               count++;
               temp = i%100;
               temp = temp/10;
               ans[count] = (char) temp;
               count++;
               temp = i%10;
               ans[count] = (char) temp;
               count++;
           }
           else
           {
               System.out.println("The Numbers Are Out of the range!!");            //Checking whether the number not going out of the rsnge
               System.out.println("The range is between 0 to 1000");                
           }
        }
        System.out.println("\nThe All Numbers between " + a + " and " + b + " are:");
       System.out.println("\n****************************************************");            
        for(i=0;i<count;i++)                //printing all numbers of array which we just stored
        {
            System.out.print(+ ans[i]);
        }
        for(i=0;i<count;i++)                //Counting number from array ans[] and assigninging to the frq araay 
        {
            switch (ans[i])
            {
                case 0: frq[0]++; break;
                case 1: frq[1]++; break;
                case 2: frq[2]++; break;
                case 3: frq[3]++; break;
                case 4: frq[4]++; break;
                case 5: frq[5]++; break;
                case 6: frq[6]++; break;
                case 7: frq[7]++; break;
                case 8: frq[8]++; break;
                case 9: frq[9]++; break;
            }
        }
        System.out.println("\n****************************************************");
       
        for(i=0;i<10;i++)
        {
            System.out.println("The Frequency of " + i + " is " + frq[i]);                      //printing the frequency 
            if(max<frq[i])
            {
                max=frq[i];
                maxNo=i;                            //finding most frequent number 
                temp=-1;
            }
            if(max==frq[i])
            {                                       // Checking numbers having same frequency as most frequency
                temp++; 
                
            }
        }
        System.out.println("\n****************************************************");
        System.out.println("The most frequent number is " + maxNo + " And it's frequency is " + max);           // printing most frequent numer and it's frequency
        if (temp>1)
            System.out.println("Other " + temp + " numbers have same frequency ");          //Printing other numbers having same frequency
        System.out.println("****************************************************");
    }   
}