/* 
 * File:   Polynomial.cpp
 * Author: User
 * 
 * Created on February 18, 2015, 8:06 PM
 */

#include "Polynomial.h"
#include<iostream>
using namespace std;



int i,j;

 Polynomial Polynomial::operator +(Polynomial obj)              //+ operator for adding two polynomials
 {
    Polynomial plus;
    plus.set_size(size+obj.get_size());
    for(i=0;i<plus.get_size();i++)
    {
        if(i<size)
        {
            plus.coefficient[i]=coefficient[i];
            plus.exponent[i]=exponent[i];
        }
        else
        {
            plus.coefficient[i]=obj.coefficient[i-size];
            plus.exponent[i]=obj.exponent[i-size];
        }
        
    }
    plus.simplify();
    return plus;
 }
 
 Polynomial Polynomial::operator *(int cons)
 {
     Polynomial obj;
     obj.set_size(size);
     for(i=0;i<size;i++)
     {
         obj.coefficient[i] = coefficient[i]*cons; 
         obj.exponent[i]=exponent[i];
     }
     
     return obj;
 }
 
  
 Polynomial Polynomial::operator -()        //negative a number 
 {
     Polynomial obj;
     obj.set_size(size);
     for (i=0;i<size;i++)
     {
         obj.coefficient[i] = -coefficient[i];
         obj.exponent[i]=exponent[i];
     }
     return obj;
 }
 
 //overloading << operator
 
/*/ namespace nsPoly
 {
    std::ostream& operator << (std::ostream& out,Polynomial& obj)
    {
        for (i=0 ; i < obj.get_size() ; i++)
        {
            if(obj.coefficient[i]!=0)
            {
                if (i== obj.get_size()-1)       //last term does not have '+'
                    out << obj.coefficient[i] << "x^" << obj.exponent[i] <<endl;
                else 
                    out << obj.coefficient[i] << "x^" << obj.exponent[i] << " + ";
            }
        }
        return out;
    }
 }
 */
 
 void Polynomial::set_val()                     //putting value in the array
 {
     for(i=0 ; i < size ; i++)
     {
         cout << "Enter the exponent of  " << i+1 << " th term : ";
         cin >> exponent[i];
         
     }
     for(i=0;i<size;i++)
     {
         cout << "Enter the coefficient of  " << i+1 <<"th term : " ;
         cin >> coefficient[i];
     }
 } 
 
 void Polynomial::get_val()                     //printing the value
 {
     for (i=0 ; i < size ; i++)
     {
         if(coefficient[i]!=0)
         {
             if (i== size-1)      //last term does not have '+'
                 cout << coefficient[i] << "x^" << exponent[i] <<endl;
             else 
                 cout << this->coefficient[i] << "x^" << this->exponent[i] << " + ";
         }
     }
 }
 
 
//returning size
 int Polynomial::get_size()
 {
     return size;
 }
 
 //setting size
 int Polynomial::set_size(int x)
 {
    size = x;
    coefficient = new int[size]; 
    exponent = new int[size];
    for(i=0 ; i < size ; i++)
    {
        coefficient[i]=0;
        exponent[i]=1;
    }
    
 }
 
 void Polynomial::simplify()
 {
    int temp;
    for(i=0;i<get_size();i++)                           //sorting in ascending order;
    {
        for(j=0;j<get_size();j++)
        {
            if(exponent[i]>exponent[j])
            {
                temp = coefficient[i];
                coefficient[i] = coefficient[j];
                coefficient[j] = temp;
                temp = exponent[i];
                exponent[i] = exponent[j];
                exponent[j] = temp;
            }
        }
    }
     //adding of simplified polynomial
 
 
     for(j=0;j<size;j++)
     {
         for (i=0;i<size;i++)
        {
             if(exponent[i]==exponent[i+1])
            {
                 coefficient[i]=coefficient[i]+coefficient[i+1];
                 coefficient[i+1]=0;
            }
        }
     }
 
 }
 

 
 