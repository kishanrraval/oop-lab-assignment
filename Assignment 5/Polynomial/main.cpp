#include <iostream>
#include <cstdlib>
#include "Polynomial.h"

using namespace std;

int main(int argc, char** argv) 
{
    int choice,temp,constant,i,ch1,ch2;
    Polynomial A[6];
    cout << "Enter total number of terms in polynomial 1 : ";
    cin >> temp ;
    A[0].set_size(temp);
    A[0].set_val();
    
    cout << "Enter total number of terms in polynomial 2 : ";
    cin >> temp ;
    A[1].set_size(temp);
    A[1].set_val();
    
    
    again:while (1)
    {
    cout << "\n\n****************************************************" << endl;
    cout << " 1) Add two polynomials" << endl;
    cout << " 2) Multiply a polynomial with a scalar value" << endl;
    cout << " 3) Negative a Polynomial" << endl;
    cout << " 4) Display a polynomial" << endl;
    cout << " 5) Exit " << endl;
    cout << "****************************************************" << endl;
    cout << " Enter Your Choice : ";
    cin >> choice;
    switch (choice)
    {
        case 1: A[2].set_size(A[0].get_size()+A[1].get_size());
                A[2] = A[0] + A[1] ;
                A[2].get_val();
                break;           //adding two polynomial 
        case 2: cout << " Enter the number you want to multiply : ";          //multiplying constant
                cin >>constant ;      
                cout << " Which polynomial you want to multiply(1 or 2) : ";
                cin >> ch1;
                if(ch1>2 || ch1 <0)
                {
                    cout << "Wrong Choice!!!" << endl;
                    goto again;
                }
                A[3].set_size(A[ch1-1].get_size());
                A[3] = A[ch1-1] * constant;  
                
                A[3].get_val();
                break;
        case 3:cout << "Which polynomial you want to make negative (1 or 2) : ";            // negative a polynomial
                cin >> ch2;
                A[4].set_size(A[(ch2-1)].get_size());
                A[4] = - A[(ch2-1)] ;
                A[4].get_val();
                break;
        
        
        case 4: cout << "which polynomial do you want to see : ";
                cin >> temp;
                
                switch (temp)
                {
                    case 1: A[0].get_val();
                            break;
                    case 2: A[1].get_val();    break;
                    default:cout << endl<< "Wrong Choice!!! Please enter a proper choice\n" ; break;                           
                }
                    break;
        case 5: exit (0); break;
        default: cout << endl<< "Wrong Choice!!! Please enter a proper choice\n" ;
        
    }
    
    }
    return 0;
}


