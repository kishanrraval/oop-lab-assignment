/* 
 * File:   Polynomial.h
 * Author: User
 *
 * Created on February 18, 2015, 8:06 PM
 */

#ifndef POLYNOMIAL_H
#define	POLYNOMIAL_H

    class Polynomial {
    public:

        Polynomial operator +(Polynomial);
        Polynomial operator *(int);
        Polynomial operator -();
       // friend std::ostream& operator << (std::ostream&,Polynomial&);
        void simAdd();
        void set_val();
        void get_val();
        int set_size(int);
        int get_size();
        void simplify();
        int lastPos();
    private:
        int* coefficient;
        int* exponent;
        int size;



    };

#endif	/* POLYNOMIAL_H */

