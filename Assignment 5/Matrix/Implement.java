import java.util.*;

class Matrix 
{
    private int row;
    private int column;
    private int mat[][];
    Scanner input = new Scanner(System.in);
    int i,j;
//Constructor
    Matrix()
    {
        System.out.print("\n Enter number of rows in the matrix : ");
        row = input.nextInt();
        System.out.print("\n Enter number of columns in the matrix : ");
        column = input.nextInt();
        mat = new int[row][column];
    }
    
    
//Consructor with size passing through two int
    Matrix(int a,int b)
    {
        row=a;
        column=b;
        mat = new int[row][column];
    }
    
    //copy constructor
    Matrix(Matrix obj)
    {
        mat = new int [obj.row][obj.column];
        row = obj.row;
        column = obj.column;
        mat = new int [obj.row][obj.column];
        for (i = 0; i < row; i++)
        {
            for (j = 0; j < column; j++)
            {
                mat[i][j] = obj.mat[i][j];
            }
        }
        mat[0][0]=obj.mat[0][0];
    }
    
//assigning values to matrix
    public void assign()
    {
        for(i=0;i<row;i++)
        {
            for (j=0;j<column;j++)
            {
                System.out.print("\n Enter value of element [" + i + "][" + j + "] : ");
                mat[i][j] = input.nextInt();
            }
        }
        System.out.println("\n Vallues assigned successfully in matrix");
            
    }
    
 //Extracting values from the matrix
    public void extract()
    {
        System.out.println("\n");
        for (i=0; i<row; i++)
        {
            for (j=0;j<column;j++) 
            {
                System.out.print( mat[i][j]+" ");
            } 
            System.out.println();           //to change the line 
        }
        System.out.println();
    } 
    
//Transpose of a matrix 
    public Matrix Transpose()
    {
        Matrix obj = new Matrix(row, column);
        for (i = 0; i < row; i++)
            for (j = 0; j < column; j++)
                obj.mat[j][i] = mat[i][j];
        return obj;
    }
    
    public int getRow()
    {
        return row;
    }
    
    public int getColumn()
    {
        return column;
    }
    
  
    
    //adding two matrix
    public Matrix add(Matrix obj)
    {
        Matrix obj1 = new Matrix(row, column);
        if(column == obj.getColumn() && row == obj.getRow())
        {
            for(i=0;i<row;i++)
            {
                for (j=0;j<column;j++)
                {   
                    obj1.mat[i][j] = mat[i][j] + obj.mat[i][j];
                }
            }
        }
        else
        {
            System.out.println("\nSize of rows and columns are different so will return 0 in every place ");
            for(i=0;i<obj1.row;i++)                                  //entering value 0 to each element of obj1
            {
                for (j=0;j<obj1.column;j++)
                {
                    obj1.mat[i][j] = 0;
                }
            }
        }
        return obj1;
    }
    
 //Multiplying two matrix
    public Matrix multiply(Matrix obj)
    { int sum = 0 ,k ;
        Matrix obj1 = new Matrix(row,obj.getColumn());
        if(row == obj.getColumn() )
        {/*n= col 1
            m=row 1
            p= row 2
            q=col 2
            */
            for ( i = 0 ; i < row ; i++ )
            {
                for ( j = 0 ; j < obj.getColumn() ; j++ )
                {   
                    for ( k = 0 ; k < obj.getRow() ; k++ )
                    {
                        sum = sum + mat[i][k]* obj.mat[k][j];
                    }
                    obj1.mat[i][j] = sum;
                    sum = 0;
                }
            }
        }
        else
        {
            System.out.println("\nNumber of rows in first Matrix and Number of columns in second matrix are not same.");
            for(i=0;i<obj1.row;i++)                                  //entering value 0 to each element of obj1
            {
                for (j=0;j<obj1.column;j++)
                {
                    obj1.mat[i][j] = 0;
                }
            }
        }
        return obj1;
    }
    
 //Multiplying with scalar
    public Matrix multiply(int scal)
    {
        Matrix obj = new Matrix(row, column);
        for(i=0;i<row;i++)                                 
            {
                for (j=0;j<column;j++)
                {
                    obj.mat[i][j] = mat[i][j] * scal;
                }
            }
        return obj;
    }
}




public class Implement {

    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int choice,temp,scalar,copy;
        System.out.println("Matrix no 1:");
        Matrix m1 = new Matrix();
        System.out.println("Values for Matrix 1");
        m1.assign();
        System.out.print("Press 1 if you want to copy Matrix 1 in matrix 2 else press 0: ");
        copy = in.nextInt();
        Matrix m2;
        if(copy == 1)
        {
             m2 = new Matrix(m1);
        }
        else
        {
            System.out.println("\nMatrix no 2:");
            m2 = new Matrix();
            System.out.println("Values for matrix 2");
            m2.assign();
        }
        
        while (true)
        {
            System.out.println("***************************************************");
            System.out.println(" 1)Display the matrix ");
            System.out.println(" 2)Find transpose of a matrix");
            System.out.println(" 3)Add two matrices");
            System.out.println(" 4)Multiply two matrices");
            System.out.println(" 5)Multiply matrix with a scalar value");
            System.out.println(" 6)Exit");
            System.out.println("***************************************************");
            System.out.print(" Enter Your Choice : ");
            choice = in.nextInt();
            switch(choice)
            {
            case 1:System.out.print("Which matrix you want to display : ");
                   temp = in.nextInt();
                    switch (temp)
                    {
                        case 1:m1.extract();break;
                        case 2:m2.extract();break;
                        default:System.out.println("Wrong Choice!!! Please enter proper choice");
                    }   break;
            case 2:
                   System.out.print("Which matrix you want to transpose : ");
                   temp = in.nextInt();
                   System.out.println("The transpose matrix is ");
                   Matrix m3;
                   switch (temp)
                   {
                       case 1:m3 = new Matrix(m1.getRow(),m1.getColumn());
                                m3 = m1.Transpose();
                                m3.extract();   break;
                       case 2: m3 = new Matrix(m2.getRow(),m2.getColumn());
                               m3 = m2.Transpose();
                               m3.extract();    break;
                       default:System.out.println("Wrong Choice!!! Please enter proper choice");
                   }
                   
                   break;
            case 3:System.out.print("The Adition of given two matrices is ");
                    Matrix m4 = new Matrix(m1.getRow(),m1.getColumn());
                    m4=m1.add(m2);
                    m4.extract();       break;
            case 4:System.out.print("The multiplication of given two matrices is ");
                    Matrix m5 = new Matrix(m1.getRow(),m2.getColumn());
                    m5=m1.multiply(m2);
                    m5.extract();       break;
            case 5: System.out.print(" Enter value of scalar which you want to multiply: ");
                    scalar = in.nextInt();
                    System.out.print("With which matrix you want to multiply : ");
                    temp = in.nextInt();
                    Matrix m6;
                    switch (temp)
                    {
                       case 1:m6=new Matrix(m1.getRow(),m1.getColumn());
                              m6 = m1.multiply(scalar);
                              m6.extract();                 break;
                       case 2:m6=new Matrix(m2.getRow(),m2.getColumn());
                              m6 = m2.multiply(scalar);
                              m6.extract();                 break;
                       default:System.out.println("Wrong Choice!!! Please enter proper choice");
                    }
                       break;
            case 6: System.exit (0);
            default : System.out.println("Wrong Choice!!! Please enter proper choice");
            }
                
        }
    }    
}
