import java.util.Scanner;
class Media      
{
    
    protected String title;             //for Title of cd or book
    protected String yearOfPublication; 
    protected int price;
    
    Media(){}       // constructor
    Media(String str, String year, int rate)
    {
        title = str ; 
        yearOfPublication = year;
        price = rate ; 
    }
    
    
}

class Book extends Media
{
    Book(){}        //constructor
    Scanner in = new Scanner(System.in);
    private String author;          //name of author
    private int no_of_pages = 0;        // number of pages in book
    
    Book(String str, String year , int rate , String auth , int pageNo)     // Parameterized constructor
    {
        super(str,year,rate);
        author = auth ; 
        no_of_pages = pageNo;
    }
    
    void display()      //printing values of object of class Media
    {
        System.out.println(" Title Name          : " + title);
        System.out.println(" Year Of Publication : " + yearOfPublication);
        System.out.println(" Price of Book       : " + price);
        System.out.println(" Name of Author      : " + author );
        System.out.println(" Pages in Book       : " + no_of_pages );
    }
    
    void SetVal()       //Setting Value from User
    {
        System.out.print(" Enter Title Name : ");
        title = in.next();
        System.out.print(" Enter Year Of Publication : ");
        yearOfPublication = in.next();
        System.out.print(" Enter Price of Book : ");
        price = in.nextInt();
        System.out.print(" Enter Name of Author : ");
        author = in.next();
        System.out.print(" Enter Pages in Book : ");
        no_of_pages = in.nextInt();
    }
    
}


class CD extends Media                 
{   CD(){}
    Scanner input = new Scanner(System.in);
    private float sizeInMB;         //Size of CD in MB
    private float playTime;         //play time of CD
    
    //Parameterized Constructor 
    CD(String str, String year , int rate , float size , float time)
    {
        super(str , year , rate);
        size = sizeInMB;
        time = playTime;
    }
    
    void display()          //Displaying Value of CD class
    {
        System.out.println(" Title Name          : " + title);
        System.out.println(" Year Of Publication : " + yearOfPublication);
        System.out.println(" Price of CD         : " + price);
        System.out.println(" Size in MB          : " + sizeInMB );
        System.out.println(" Play Time           : " + playTime );
    }
    
    void setVal()           // Setting value by user
    {
        System.out.print(" Enter Title Name : " );
        title = input.next();
        System.out.print(" Eneter Year Of Publication : " );
        yearOfPublication = input.next();
        System.out.print(" Enter Price of CD : " );
        price = input.nextInt();
        System.out.print(" Enter Size in MB : " );
        sizeInMB = input.nextFloat();
        System.out.print(" Enter Play Time : " );
        playTime = input.nextFloat();
    }
}

public class JavaEx2
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        Book Obj1 = new Book();
        CD Obj2 = new CD();
        int count = 0, count1 = 0;
        int choice,choice1;
        do
        {
            System.out.println("********************************************");
            System.out.println(" 1) Change Value  ");
            System.out.println(" 2) Display A value  ");
            System.out.println(" 3) Exit ");
            System.out.println("********************************************");
            System.out.print(" Enter Your Choice : ");
            choice = input.nextInt();
        
            switch(choice)
            {
                case 1: System.out.println(" 1) Book ");			//asking for which you want to add details
                    System.out.println(" 2) CD");
                    System.out.print("Enter Your Choice : ");
                    choice1 = input.nextInt();
                    switch(choice1)
                    {
                        case 1: Obj1.SetVal();
                                count++;
                                break;
                        case 2: Obj2.setVal();
                                count1++;
                                break;
                        default: System.out.println("Wrong Choice!!! Please Enter Correct Choice.");
                    }
                    break;
                case 2: System.out.println(" 1) Book ");		// asking for which you want to display
                    System.out.println(" 2) CD");
                    System.out.print("Enter Your Choice : ");
                    choice = input.nextInt();
                    switch(choice)
                    {
                        case 1: if(count>0)
                                    Obj1.display();
                                else
                                    System.out.println(" Please Set a value first and then display.");
                                break;
                        case 2: if(count1>0)
                                    Obj2.display();
                                else
                                    System.out.println(" Please Set a value first and then display.");
                                break;
                        default: System.out.println("Wrong Choice!!! Please Enter Correct Choice.");
                    }
                    break;
                case 3: System.exit(0); 		
                default:System.out.println("Wrong Choice!!! Please Enter Correct Choice.");
            }
        }while(true);
    }    
}