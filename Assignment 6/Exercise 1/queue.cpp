
#include <cstdlib>
#include <iostream>

using namespace std;

namespace nsQueue
{
    class Queue {
public:
    Queue(int);						
    ~Queue();
    bool operator +(int);
    void getVal();
    friend std::ostream& operator<< (std::ostream& out,const Queue& obj)			//operator overloading <<
    {
        int i;
        if (obj.tail == 0)
        out << "The Queue is empty \n" ;
        out << "The elements in queue are : ";
        for (i = 0 ; i < obj.tail ; i++)
        {
            out << obj.array[i] << "  ";
        }
        return out;
    }
    int operator --();
    int operator --(int);
private:
    int *array;					//array for storing values
    int tail = 0,size = 0;

};
}

using namespace nsQueue;
Queue::Queue(int x)     //constructor
{
    size = x;
    array = new int[size];
    tail = 0;
}

Queue::~Queue()         //destructor
{ }

bool Queue::operator +(int value)       //enqueue a number
{
    if (tail == size)
    {
        return false;
    }
    else
    {

        array[tail]= value;
        tail++;
        return true;
    }
}



int Queue::operator --()        //dequeue a number
{
    if (tail == 0)
       return -999;
    int temp = 0;
    temp =array[0];

    for (int i = 0 ;i < tail-1 ; i++ )
    {
        array[i] = array[i+1];
    }
    tail--;
   return temp;
}

int main(int argc, char** argv) {
    int choice,in,temp;
    bool check;
    cout << "Enter maximum length of queue : ";				//getting size of array
    cin >> temp ;
    Queue object(temp);										//creating an object
    while (1)
    {
        cout << "\n****************************************" << endl;
        cout << " 1) Enqueue A number " << endl;
        cout << " 2) Dequeue A number "  << endl;
        cout << " 3) Display List " << endl;
        cout << " 4) Exit " << endl;
        cout << "***************************************" << endl;
        cout << " Enter your Choice : " ;
        cin >> choice;
        switch (choice)
        {
            case 1: cout << "Enter a number you want to Enqueue : ";
                    cin >> in ;
                    check = (object+in);			//passing value and storing return bool value to check
                    if (check == true)				//prints is stored successfully
                        cout << "The Number " << in << " has been stores successfully in the queue" << endl;
                    else							//prints if number not stored
                        cout << " The array is full " << endl;
                    break;
            case 2: cout << "The number " << --object << " has been Dequeued Successfully" << endl;		//Dequeue a number and printing it.
                    break;
            case 3: cout << object;						//Displaying Object
                    break;
            case 4: goto end;
            default: cout << "wrong Choice!!! Enter a proper choice " << endl;
        }
    }
    end:return 0;
}
