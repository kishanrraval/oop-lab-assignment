import java.util.Scanner;
class Point 
{
    private int size;       //for size of array(dimension)
    private int[] data;     //array for saving data 
    
    Point(int x)            //Constructor
    {
        size = x;      
        data = new int[size];       //Allocating memory
    }
    
    Point(Point obj)            //copy constructor
    {
        int i;
        size = obj.size;
        data = new int[size];
        for(i=0 ; i < size ; i++)
        {
            data[i] = obj.data[i] ; 
        }
    }
    
    void setVal()				//Setting Value
    {
        Scanner input = new Scanner(System.in);
        int i;
        for(i=0 ; i<size ; i++)
        {
            System.out.print(" Enter Value in dimension " + (i+1) + " : ");
            data[i] = input.nextInt();
        }
    }
    
    void getVal()			//Getting Value
    {
        int i;
		System.out.print(" The Value of Point is : ");
        for(i=0 ; i<size ; i++)
        {
            
            System.out.print(" " + data[i] + " ");
        }
        System.out.println("");
    }
    
    boolean isEqual(Point obj)			//Checking whether two objects are same or not
    {
        int i,count = 0;
        if (size != obj.size)		//checking size
            count++;
        for(i=0 ; i<size ; i++)		//Checking Value
        {
            if (data[i]!=obj.data[i])
                count++;
        }
        if(count==0)
            return true;
        return false;
    }
    
    
}

public class JavaEx3 
{

  
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        int choice,choice1,i,ObjectNo,choice2,count = 0,temp;
        boolean ans;
        
        System.out.print(" Enter Number of maximum objects you want to make : ");
        ObjectNo = input.nextInt();
        
        Point []array = new Point[ObjectNo];
        
        while(true)
		{
			System.out.println("****************************************");
			System.out.println(" 1) Add Value");
			System.out.println(" 2) Display A value");
			System.out.println(" 3) Copy A Value");
			System.out.println(" 4) Check if Point are same of not ");
			System.out.println(" 5) Exit");
			System.out.println("****************************************");
			System.out.print(" Enter Your Choice : ");
			choice = input.nextInt();
			switch(choice)
			{
				case 1: System.out.print(" Enter Number of dimensions in point : ");		//adding value
                    temp = input.nextInt();
                    array[count] = new Point(temp);
                    array[count].setVal();
                    count++;
                    System.out.println("\n The point has been Saved Successfully to the place " + count);
                    break;
				case 2: System.out.print(" Enter Point you Want to See : ");				//Displaying Value
                    choice1 = input.nextInt();
                    array[choice1-1].getVal();
                    break;
				case 3: System.out.print(" Enter Point Which you want to copy : ");			//Copying a value
                    choice1 = input.nextInt();
                    array[count] = new Point(array[choice1-1]); 
					count++;
					System.out.println("\n The point has been Saved Successfully to the place " + count);
                    
                    break;
				case 4: System.out.print("Enter First Point number for compare : ");		//Checking whether two points are same or not 
                    choice1 = input.nextInt();
                    System.out.print("Enter Second Point number for compare : ");
                    choice2 = input.nextInt();
                    ans = array[choice1-1].isEqual(array[choice2-1]);
                    if (ans == true)
                        System.out.println(" Both points are Equal ");
					else
						System.out.println(" Both points are not Equal");
                    break;
				case 5: System.exit(0);
                    break;
			}
        }
    }
}